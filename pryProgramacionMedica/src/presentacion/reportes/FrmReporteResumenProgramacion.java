package presentacion.reportes;

import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.swing.JRViewer;
import util.Funciones;
import util.Reportes;
import javax.swing.JDesktopPane;
import negocio.DptoHospital;
import negocio.Servicios;
import presentacion.FrmBuscarMedicoReporte;

public class FrmReporteResumenProgramacion extends javax.swing.JInternalFrame {

    public String nombreArchivoReporte = "";

    public FrmReporteResumenProgramacion(JDesktopPane panelContenedor) {
        initComponents();
        this.vistaReporte.setVisible(false);
        this.vistaReporte.setLayout(new BorderLayout());
        this.setSize(panelContenedor.getWidth(), panelContenedor.getHeight());   
        llenarComboDpto();
    }
    
    private void llenarComboDpto() {
        try {
            new DptoHospital().llenarCombo(cboDptoHosp);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }
    
    public boolean validarDatos(){
        if(cboServicios.getSelectedItem() == null){
            Funciones.mensajeAdvertencia("Seleccione un SERVICIO", "¡ADVERTENCIA!");
            cboServicios.requestFocus();
            return false;
        }else if(txtAnio.getText().isEmpty()){
            Funciones.mensajeAdvertencia("Escriba el AÑO del Informe", "¡ADVERTENCIA!");
            txtAnio.requestFocus();
            return false;
        }else if(cboMes.getSelectedItem().toString().equalsIgnoreCase("")){
                Funciones.mensajeAdvertencia("Seleccione el MES del Informe", "¡ADVERTENCIA!");
                cboMes.requestFocus();
            return false;
        }
        return true;
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        cboDptoHosp = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        cboServicios = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        btnVerInforme = new javax.swing.JButton();
        txtAnio = new javax.swing.JTextField();
        cboMes = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        vistaReporte = new javax.swing.JDesktopPane();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Resumen de la Porgramación de los Turnos del Trabajo Médico");

        jPanel2.setBackground(new java.awt.Color(70, 99, 138));

        jPanel3.setBackground(new java.awt.Color(247, 254, 255));

        jPanel4.setBackground(new java.awt.Color(247, 254, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DEL AREA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Maiandra GD", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        cboDptoHosp.setBackground(new java.awt.Color(247, 254, 255));
        cboDptoHosp.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboDptoHosp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboDptoHospActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel13.setText("DEPARTAMENTO:");

        jLabel14.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel14.setText("SERVICIO:");
        jLabel14.setToolTipText("");

        cboServicios.setBackground(new java.awt.Color(247, 254, 255));
        cboServicios.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboDptoHosp, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(cboServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboDptoHosp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboServicios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(247, 254, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FECHA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Black", 1, 12), new java.awt.Color(0, 153, 153))); // NOI18N

        btnVerInforme.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        btnVerInforme.setText("Ver Reporte");
        btnVerInforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerInformeActionPerformed(evt);
            }
        });

        cboMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));

        jLabel15.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel15.setText("AÑO");
        jLabel15.setToolTipText("");

        jLabel16.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel16.setText("MES");
        jLabel16.setToolTipText("");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cboMes, 0, 155, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnVerInforme, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVerInforme))
                .addContainerGap())
        );

        vistaReporte.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout vistaReporteLayout = new javax.swing.GroupLayout(vistaReporte);
        vistaReporte.setLayout(vistaReporteLayout);
        vistaReporteLayout.setHorizontalGroup(
            vistaReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        vistaReporteLayout.setVerticalGroup(
            vistaReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 347, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(vistaReporte)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 154, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(17, 17, 17)
                .addComponent(vistaReporte)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVerInformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerInformeActionPerformed
        if(this.validarDatos() == false){
                return; // Detiene el programa
        }
        
        try {
            Integer mes = 0;
            String nombreMes = cboMes.getSelectedItem().toString();
            
            if(nombreMes.equalsIgnoreCase("Enero")){
                mes = 1;
            }else if(nombreMes.equalsIgnoreCase("Febrero")){
                mes = 2;
            }else if(nombreMes.equalsIgnoreCase("Marzo")){
                mes = 3;
            }else if(nombreMes.equalsIgnoreCase("Abril")){
                mes = 4;
            }else if(nombreMes.equalsIgnoreCase("Mayo")){
                mes = 5;
            }else if(nombreMes.equalsIgnoreCase("Junio")){
                mes = 6;
            }else if(nombreMes.equalsIgnoreCase("Julio")){
                mes = 7;
            }else if(nombreMes.equalsIgnoreCase("Agosto")){
                mes = 8;
            }else if(nombreMes.equalsIgnoreCase("Septiembre")){
               mes =9;
            }else if(nombreMes.equalsIgnoreCase("Octubre")){
                mes = 10;
            }else if(nombreMes.equalsIgnoreCase("Noviembre")){
                mes = 11;
            }else if(nombreMes.equalsIgnoreCase("Diciembre")){
                mes = 12;
            }else if(nombreMes.equalsIgnoreCase("")){
                Funciones.mensajeAdvertencia("Seleccione un MES", "¡ADVERTENCIA!");
                cboMes.requestFocus();
                return;
            }
            /*configurar los parametro para el reporte (ESTO SOLO SE HACE CUANDO EL REPORTE TIENE PARAMETROS)*/

            Integer anio = Integer.parseInt(this.txtAnio.getText());
            Integer codigoDep=DptoHospital.listaDptoHosp.get(cboDptoHosp.getSelectedIndex()).getCodigoDptoHosp();
            Integer codigoServicio =Servicios.listaServicios.get(cboServicios.getSelectedIndex()).getIdServicio();
            
            Map<String, Object> parametros = new HashMap<String, Object>();

            parametros.put("p_anio", anio);
            parametros.put("p_mes", mes);
            parametros.put("p_id_dpto", codigoDep);
            parametros.put("p_id_servicio", codigoServicio);

            /*configurar los parametro para el reporte (ESTO SOLO SE HACE CUANDO EL REPORTE TIENE PARAMETROS)*/
            JRViewer objReporte = new Reportes().reporteInterno(nombreArchivoReporte, parametros);
            this.vistaReporte.setVisible(true);
            this.vistaReporte.removeAll(); // Para que si es que hubiera un reporte lo removiera para que muestre otro
            this.vistaReporte.add(objReporte);

        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }//GEN-LAST:event_btnVerInformeActionPerformed

    private void cboDptoHospActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboDptoHospActionPerformed
        if (this.cboDptoHosp.getSelectedIndex() >= 0) {
            int codigoDpto = DptoHospital.listaDptoHosp.get(cboDptoHosp.getSelectedIndex()).getCodigoDptoHosp();
            try {
                new Servicios().cargarComboParaDpto(cboServicios, codigoDpto);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "¡ERROR!");
            }
        }
    }//GEN-LAST:event_cboDptoHospActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVerInforme;
    private javax.swing.JComboBox<String> cboDptoHosp;
    private javax.swing.JComboBox<String> cboMes;
    private javax.swing.JComboBox<String> cboServicios;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField txtAnio;
    public javax.swing.JDesktopPane vistaReporte;
    // End of variables declaration//GEN-END:variables
}
