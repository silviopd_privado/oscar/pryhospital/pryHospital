package presentacion.reportes;

import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.swing.JRViewer;
import util.Funciones;
import util.Reportes;
import javax.swing.JDesktopPane;
import presentacion.FrmBuscarMedicoReporte;

public class FrmReporteProgramacionIndividual extends javax.swing.JInternalFrame {

    public String nombreArchivoReporte = "";

    public FrmReporteProgramacionIndividual(JDesktopPane panelContenedor) {
        initComponents();
        this.vistaReporte.setVisible(false);
        this.vistaReporte.setLayout(new BorderLayout());
        this.setSize(panelContenedor.getWidth(), panelContenedor.getHeight());   
    }
    
    public boolean validarDatos(){
        if(lblIdMedico.getText().isEmpty()){
            Funciones.mensajeAdvertencia("Seleccione un MÉDICO", "¡ADVERTENCIA!");
            btnBuscarMedico.doClick();
            return false;
        }else if(txtAnio.getText().isEmpty()){
            Funciones.mensajeAdvertencia("Escriba el AÑO del Informe", "¡ADVERTENCIA!");
            txtAnio.requestFocus();
            return false;
        }else if(cboMes.getSelectedItem().toString().equalsIgnoreCase("")){
                Funciones.mensajeAdvertencia("Seleccione el MES del Informe", "¡ADVERTENCIA!");
                cboMes.requestFocus();
                return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        lblNombres = new javax.swing.JTextField();
        btnBuscarMedico = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        lblIdMedico = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        lblAP_paternos = new javax.swing.JTextField();
        lblAP_maternos = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnVerInforme = new javax.swing.JButton();
        txtAnio = new javax.swing.JTextField();
        cboMes = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        vistaReporte = new javax.swing.JDesktopPane();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Reporte de Porgramación de los Turnos del Trabajo Médico");

        jPanel2.setBackground(new java.awt.Color(70, 99, 138));

        jPanel3.setBackground(new java.awt.Color(247, 254, 255));

        jPanel4.setBackground(new java.awt.Color(247, 254, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DEL MÉDICO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Maiandra GD", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        jLabel7.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel7.setText("NOMBRES:");

        lblNombres.setEditable(false);
        lblNombres.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lblNombres.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnBuscarMedico.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnBuscarMedico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/proveedor.png"))); // NOI18N
        btnBuscarMedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarMedicoActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel8.setText("ID:");

        lblIdMedico.setEditable(false);
        lblIdMedico.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblIdMedico.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel9.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel9.setText("Ap. PATERNOS:");

        lblAP_paternos.setEditable(false);
        lblAP_paternos.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lblAP_paternos.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        lblAP_maternos.setEditable(false);
        lblAP_maternos.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        lblAP_maternos.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel12.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel12.setText("Ap. MATERNOS:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNombres, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblAP_paternos, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lblAP_maternos, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBuscarMedico)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnBuscarMedico)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jLabel9)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAP_maternos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblAP_paternos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(247, 254, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FECHA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Black", 1, 12), new java.awt.Color(0, 153, 153))); // NOI18N

        btnVerInforme.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        btnVerInforme.setText("Ver Reporte");
        btnVerInforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerInformeActionPerformed(evt);
            }
        });

        cboMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));

        jLabel16.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel16.setText("MES:");
        jLabel16.setToolTipText("");

        jLabel17.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel17.setText("AÑO:");
        jLabel17.setToolTipText("");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAnio, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(130, 130, 130))
                    .addComponent(cboMes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(btnVerInforme, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(2, 2, 2)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnVerInforme))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        vistaReporte.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout vistaReporteLayout = new javax.swing.GroupLayout(vistaReporte);
        vistaReporte.setLayout(vistaReporteLayout);
        vistaReporteLayout.setHorizontalGroup(
            vistaReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        vistaReporteLayout.setVerticalGroup(
            vistaReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 394, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(vistaReporte)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 72, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(vistaReporte)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnVerInformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerInformeActionPerformed
        if(this.validarDatos() == false){
                return; // Detiene el programa
        }
        
        try {
            Integer mes = 0;
            String nombreMes = cboMes.getSelectedItem().toString();
            
            if(nombreMes.equalsIgnoreCase("Enero")){
                mes = 1;
            }else if(nombreMes.equalsIgnoreCase("Febrero")){
                mes = 2;
            }else if(nombreMes.equalsIgnoreCase("Marzo")){
                mes = 3;
            }else if(nombreMes.equalsIgnoreCase("Abril")){
                mes = 4;
            }else if(nombreMes.equalsIgnoreCase("Mayo")){
                mes = 5;
            }else if(nombreMes.equalsIgnoreCase("Junio")){
                mes = 6;
            }else if(nombreMes.equalsIgnoreCase("Julio")){
                mes = 7;
            }else if(nombreMes.equalsIgnoreCase("Agosto")){
                mes = 8;
            }else if(nombreMes.equalsIgnoreCase("Septiembre")){
                mes =9;
            }else if(nombreMes.equalsIgnoreCase("Octubre")){
                mes = 10;
            }else if(nombreMes.equalsIgnoreCase("Noviembre")){
                mes = 11;
            }else if(nombreMes.equalsIgnoreCase("Diciembre")){
                mes = 12;
            }
            
            /*configurar los parametro para el reporte (ESTO SOLO SE HACE CUANDO EL REPORTE TIENE PARAMETROS)*/

            Integer anio = Integer.parseInt(this.txtAnio.getText());
            Integer id_medico = Integer.parseInt(this.lblIdMedico.getText());

            Map<String, Object> parametros = new HashMap<String, Object>();

            parametros.put("p_anio", anio);
            parametros.put("p_mes", mes);
            parametros.put("p_id_medico", id_medico);

            /*configurar los parametro para el reporte (ESTO SOLO SE HACE CUANDO EL REPORTE TIENE PARAMETROS)*/
            JRViewer objReporte = new Reportes().reporteInterno(nombreArchivoReporte, parametros);
            this.vistaReporte.setVisible(true);
            this.vistaReporte.removeAll(); // Para que si es que hubiera un reporte lo removiera para que muestre otro
            this.vistaReporte.add(objReporte);

        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }//GEN-LAST:event_btnVerInformeActionPerformed

    private void btnBuscarMedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarMedicoActionPerformed
        FrmBuscarMedicoReporte objFrm = new FrmBuscarMedicoReporte(null, true);
        objFrm.setTitle("Buscar Médico");
        objFrm.setVisible(true);

        int accioResultado = objFrm.accion;
        if (accioResultado == 0) {
            return;
        }
        int filaSeleccionada = objFrm.tblResultado.getSelectedRow();

        String idMedico = objFrm.tblResultado.getValueAt(filaSeleccionada, 0).toString();
        String dni = objFrm.tblResultado.getValueAt(filaSeleccionada, 1).toString();
        String apellidoPaterno = objFrm.tblResultado.getValueAt(filaSeleccionada, 2).toString();
        String apellidoMaterno = objFrm.tblResultado.getValueAt(filaSeleccionada, 3).toString();
        String nombres = objFrm.tblResultado.getValueAt(filaSeleccionada, 4).toString();
        String colegiatura = objFrm.tblResultado.getValueAt(filaSeleccionada, 5).toString();
        String rne = objFrm.tblResultado.getValueAt(filaSeleccionada, 6).toString();

        lblIdMedico.setText(idMedico);
        
        lblNombres.setText(nombres);
        lblAP_paternos.setText(apellidoPaterno);
        lblAP_maternos.setText(apellidoMaterno);
    }//GEN-LAST:event_btnBuscarMedicoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarMedico;
    private javax.swing.JButton btnVerInforme;
    private javax.swing.JComboBox<String> cboMes;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    public javax.swing.JTextField lblAP_maternos;
    public javax.swing.JTextField lblAP_paternos;
    public javax.swing.JTextField lblIdMedico;
    public javax.swing.JTextField lblNombres;
    private javax.swing.JTextField txtAnio;
    public javax.swing.JDesktopPane vistaReporte;
    // End of variables declaration//GEN-END:variables
}
