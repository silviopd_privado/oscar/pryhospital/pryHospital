
package presentacion;

import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.swing.JRViewer;
import util.Funciones;
import util.Reportes;

public class FrmReporteMedico extends javax.swing.JInternalFrame {

    public String nombreArchivoReporte = "";

    public FrmReporteMedico() {
        initComponents();
        this.vistaReporte.setVisible(false);
        this.vistaReporte.setLayout(new BorderLayout());
        rbTodos.setSelected(true);

        btnBuscar.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        vistaReporte = new javax.swing.JDesktopPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtRuc = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnVerInforme = new javax.swing.JButton();
        rbProveedor = new javax.swing.JRadioButton();
        rbTodos = new javax.swing.JRadioButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        vistaReporte.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout vistaReporteLayout = new javax.swing.GroupLayout(vistaReporte);
        vistaReporte.setLayout(vistaReporteLayout);
        vistaReporteLayout.setHorizontalGroup(
            vistaReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 809, Short.MAX_VALUE)
        );
        vistaReporteLayout.setVerticalGroup(
            vistaReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 305, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Especialidad", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setText("Especialidad");

        txtRuc.setEditable(false);
        txtRuc.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtRuc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRucKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRucKeyTyped(evt);
            }
        });

        btnBuscar.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cargos.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnVerInforme.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        btnVerInforme.setText("Ver Reporte");
        btnVerInforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerInformeActionPerformed(evt);
            }
        });

        rbProveedor.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        rbProveedor.setText("De una sola especialidad");
        rbProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbProveedorActionPerformed(evt);
            }
        });

        rbTodos.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        rbTodos.setText("Todas las especialidades");
        rbTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTodosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(rbTodos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbProveedor))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(jLabel6)
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtRuc, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscar))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(347, 347, 347)
                                .addComponent(btnVerInforme, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(42, 42, 42))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbProveedor)
                    .addComponent(rbTodos))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtRuc)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnVerInforme)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(vistaReporte)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(vistaReporte)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtRucKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRucKeyPressed

    }//GEN-LAST:event_txtRucKeyPressed

    private void txtRucKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRucKeyTyped

    }//GEN-LAST:event_txtRucKeyTyped

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
FrmBuscarMedico objFrm = new FrmBuscarMedico(null, true);
        objFrm.setTitle("Buscar proveedor");
        objFrm.setVisible(true);

        int accionResultado = objFrm.accion;

        if (accionResultado == 0){
            return; //Detengo el programa
        }

        //Funciones.mensajeInformacion(String.valueOf(accionResultado), "Acción");

        int filaSeleccionada = objFrm.tblResultado.getSelectedRow();

        String ruc = objFrm.tblResultado.getValueAt(filaSeleccionada, 0).toString();
        String rz = objFrm.tblResultado.getValueAt(filaSeleccionada, 1).toString();
        String dir = objFrm.tblResultado.getValueAt(filaSeleccionada, 2).toString();
        String tel = objFrm.tblResultado.getValueAt(filaSeleccionada, 3).toString();

        this.txtRuc.setText(ruc);
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnVerInformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerInformeActionPerformed
try {
            /*configurar los parametro para el reporte (ESTO SOLO SE HACE CUANDO EL REPORTE TIENE PARAMETROS)*/
            
            String especialidad = "";
            
            if(rbTodos.isSelected()){
                especialidad = "*";
            }else{
                especialidad = this.txtRuc.getText();
            }
            
            Map<String, Object> parametros = new HashMap<String, Object>();
            
            parametros.put("ps_especialidad", especialidad);
            
            /*configurar los parametro para el reporte (ESTO SOLO SE HACE CUANDO EL REPORTE TIENE PARAMETROS)*/
            
            JRViewer objReporte = new Reportes().reporteInterno(nombreArchivoReporte, parametros);
             this.vistaReporte.setVisible(true);
            this.vistaReporte.removeAll(); // Para que si es que hubiera un reporte lo removiera para que muestre otro 
            this.vistaReporte.add(objReporte);
           
                    
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }//GEN-LAST:event_btnVerInformeActionPerformed

    private void rbProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbProveedorActionPerformed
        btnBuscar.setEnabled(true);
        btnBuscar.doClick();
    }//GEN-LAST:event_rbProveedorActionPerformed

    private void rbTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTodosActionPerformed
        btnBuscar.setEnabled(false);
    }//GEN-LAST:event_rbTodosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnVerInforme;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton rbProveedor;
    private javax.swing.JRadioButton rbTodos;
    public javax.swing.JTextField txtRuc;
    public javax.swing.JDesktopPane vistaReporte;
    // End of variables declaration//GEN-END:variables
}
