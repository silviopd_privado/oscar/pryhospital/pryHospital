package presentacion;

import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import negocio.Cargo;
import negocio.DptoHospital;
import util.Funciones;

public class FrmCargoListado extends javax.swing.JInternalFrame {

    ResultSet resultado;

    public String operacion; //Agregar o editar los articulos
    public String grabadoCorrectamente = "no";

    public FrmCargoListado() {
        initComponents();
        cargarLista();
        listar();
    }

    private void cargarLista() {
        try {
            Cargo objCargo = new Cargo();
            resultado = objCargo.listar();
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }

    private void listar() {
        try {
            String alineacion[] = {"C", "I"};
            int ancho[] = {50, 300};
            //Funciones.llenarTabla(tblListado, resultado);
            Funciones.llenarTabla(tblListado, resultado, ancho, alineacion);
//            Funciones.llenarTablaBusqueda(tblListado, resultado, ancho, alineacion, null, this.txtValorBuscado.getText());
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        tbOpciones = new javax.swing.JToolBar();
        btnAgregar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        btnEditar = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        btnEliminar = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        btnRefrescar = new javax.swing.JButton();
        jSeparator7 = new javax.swing.JToolBar.Separator();
        jSeparator8 = new javax.swing.JToolBar.Separator();
        btnSalir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblListado = new javax.swing.JTable(){
            public boolean isCellEditable(int fila, int columna){
                return false;
            }
        };
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();

        jLabel1.setText("jLabel1");

        jTextField1.setText("jTextField1");

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Listado de Cargos");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        tbOpciones.setOrientation(javax.swing.SwingConstants.VERTICAL);
        tbOpciones.setRollover(true);

        btnAgregar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/AGREGAR.png"))); // NOI18N
        btnAgregar.setFocusable(false);
        btnAgregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });
        tbOpciones.add(btnAgregar);
        tbOpciones.add(jSeparator1);
        tbOpciones.add(jSeparator2);

        btnEditar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/EDITAR.png"))); // NOI18N
        btnEditar.setFocusable(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        tbOpciones.add(btnEditar);
        tbOpciones.add(jSeparator3);
        tbOpciones.add(jSeparator4);

        btnEliminar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ELIMINAR.png"))); // NOI18N
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        tbOpciones.add(btnEliminar);
        tbOpciones.add(jSeparator5);
        tbOpciones.add(jSeparator6);

        btnRefrescar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnRefrescar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ACTUALIZAR.png"))); // NOI18N
        btnRefrescar.setFocusable(false);
        btnRefrescar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefrescar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarActionPerformed(evt);
            }
        });
        tbOpciones.add(btnRefrescar);
        tbOpciones.add(jSeparator7);
        tbOpciones.add(jSeparator8);

        btnSalir.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/SALIR.png"))); // NOI18N
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        tbOpciones.add(btnSalir);

        tblListado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblListado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblListadoMouseClicked(evt);
            }
        });
        tblListado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblListadoKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblListado);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Brush Script MT", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 153, 153));
        jLabel2.setText("Listado de Cargos");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tbOpciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tbOpciones, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        FrmCargoAE objAE = new FrmCargoAE(null, true);
        objAE.setAccion(0);
//        System.out.println("accion agregar: "+objAE.getAccion());
        objAE.setTitle("Agregar datos del Cargo");
        objAE.operacion = "AGREGAR";
        objAE.setVisible(true); //Se detiene la operación hasta que se cierre el formulario

        //El bloque de código que sigue se ejecuta recíen al cerrar e formulario
        //Funciones.mensajeInformacion("Se ha cerrado el Frm. El Frm de agregar indicó el mensaje: " + objAgregarEditar.grabadoCorrectamente, "Mensaje de Información");
        if (objAE.grabadoCorrectamente.equalsIgnoreCase("si")) {
            //El Frm agregar/editar ha grabado correctamente
            this.cargarLista();
            this.listar();
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        int filaSeleccionada = this.tblListado.getSelectedRow();
        if (filaSeleccionada < 0) {
            //quiere decir que no se ha seleccionado una fila
            Funciones.mensajeError("Debe seleccionar una fila", "¡VERIFICAR!");
            return;
        }

        int codigoCargo = Integer.parseInt(this.tblListado.getValueAt(filaSeleccionada, 0).toString());

        //Funciones.mensajeInformacion(String.valueOf(codigoArticulo), "Articulo eliminado");
        int respuestaUsr = Funciones.mensajeConfirmacion("Desea ELIMINAR el CARGO seleccionado", "¡CONFIRMAR!");
        if (respuestaUsr == 1) {
            return; //Se detiene el programa porque no elimina nada
        }

        try {
            Cargo obj = new Cargo();
            boolean resultado = obj.eliminar(codigoCargo);
            if (resultado == true) {  //Si ha eliminado el registro correctamente entonces:
                cargarLista();
                listar();

            }
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
//       this.txtValorBuscado.setPreferredSize(new Dimension(300, 25));

//        tbOpciones.add(txtValorBuscado,2);
    }//GEN-LAST:event_formInternalFrameActivated

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        FrmCargoAE objAgregarEditar = new FrmCargoAE(null, true);
        objAgregarEditar.setTitle("Editar datos del Cargo");
        objAgregarEditar.setAccion(1);
//        System.out.println("accion editar: "+objAgregarEditar.getAccion());

        objAgregarEditar.operacion = "EDITAR";
        //Capturar el codigo de la lista para editar
        int filaSeleccionada = this.tblListado.getSelectedRow();
        if (filaSeleccionada < 0) {
            //quiere decir que no se ha seleccionado una fila
            Funciones.mensajeError("Debe seleccionar una fila", "¡VERIFICAR!");
            return;
        }

        int codigoCargo = Integer.parseInt(this.tblListado.getValueAt(filaSeleccionada, 0).toString());
        objAgregarEditar.leerDatos(codigoCargo);
        objAgregarEditar.setVisible(true);

        if (objAgregarEditar.grabadoCorrectamente.equalsIgnoreCase("si")) {
            //El Frm agregar/editar ha grabado correctamente
            this.cargarLista();
            this.listar();
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnRefrescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarActionPerformed

    }//GEN-LAST:event_btnRefrescarActionPerformed

    private void tblListadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblListadoMouseClicked
        if (evt.getClickCount() == 2) {
            btnEditar.doClick();
        }
    }//GEN-LAST:event_tblListadoMouseClicked

    private void tblListadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblListadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            this.btnEliminar.doClick();
        } else if (evt.getKeyCode() == KeyEvent.VK_F5) {
            this.btnRefrescar.doClick();
        } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.btnSalir.doClick();
        }
    }//GEN-LAST:event_tblListadoKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnRefrescar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JToolBar.Separator jSeparator8;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JToolBar tbOpciones;
    private javax.swing.JTable tblListado;
    // End of variables declaration//GEN-END:variables
}
