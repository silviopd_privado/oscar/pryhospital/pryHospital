package presentacion;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import presentacion.reportes.FrmReporteProgramacionIndividual;
import presentacion.reportes.FrmReporteResumenProgramacion;
import util.Funciones;
import util.ImagenFondo;

public class FrmMenuPrincipalAdm extends javax.swing.JFrame {

    public FrmMenuPrincipalAdm() {
        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setTitle(Funciones.NOMBRE_SOFTWARE + "[ Menú principal ]");
        this.lblNombreUsuario.setText("Usuario: " + Funciones.USUARIO_LOGUEADO_NOMBRE);
        this.lblFecha.setText("Fecha: " + Funciones.obtenerFechaActual());
        this.dpnContenedor.setBorder(new ImagenFondo("extendido"));
        validarUsuario();
        mnuMedica.setVisible(false);
        mnuRetenes.setVisible(false);
        mnuPagos.setVisible(false);
    }
    
    private void validarUsuario(){
        if(Funciones.CODIGO_TIPO_USUARIO == 2){
            mnuArchivo.setEnabled(false);
            mnuTransacciones.setEnabled(false);
        }else{
            mnuArchivo.setEnabled(true);
            mnuTransacciones.setEnabled(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jToolBar2 = new javax.swing.JToolBar();
        jLabel2 = new javax.swing.JLabel();
        jSeparator13 = new javax.swing.JToolBar.Separator();
        jSeparator14 = new javax.swing.JToolBar.Separator();
        lblNombreUsuario = new javax.swing.JLabel();
        jSeparator15 = new javax.swing.JToolBar.Separator();
        jSeparator16 = new javax.swing.JToolBar.Separator();
        lblFecha = new javax.swing.JLabel();
        dpnContenedor = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnuArchivo = new javax.swing.JMenu();
        mnuActividades = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        mnuTransacciones = new javax.swing.JMenu();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem22 = new javax.swing.JMenuItem();
        mnuMedica = new javax.swing.JMenuItem();
        mnuRetenes = new javax.swing.JMenuItem();
        mnuPagos = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        mnuProgramacionIndividual = new javax.swing.JMenuItem();
        mnuResumenProgramacion = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();

        jLabel3.setText("jLabel3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jToolBar2.setRollover(true);

        jLabel2.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bienvenido.png"))); // NOI18N
        jLabel2.setText("¡BIENVENIDO!");
        jToolBar2.add(jLabel2);
        jToolBar2.add(jSeparator13);
        jToolBar2.add(jSeparator14);

        lblNombreUsuario.setFont(new java.awt.Font("Maiandra GD", 0, 14)); // NOI18N
        lblNombreUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/usuario2.png"))); // NOI18N
        lblNombreUsuario.setText("Nombre de Usuario");
        jToolBar2.add(lblNombreUsuario);
        jToolBar2.add(jSeparator15);
        jToolBar2.add(jSeparator16);

        lblFecha.setFont(new java.awt.Font("Maiandra GD", 0, 14)); // NOI18N
        lblFecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/calendario.png"))); // NOI18N
        lblFecha.setText("Fecha");
        jToolBar2.add(lblFecha);

        dpnContenedor.setToolTipText("");

        javax.swing.GroupLayout dpnContenedorLayout = new javax.swing.GroupLayout(dpnContenedor);
        dpnContenedor.setLayout(dpnContenedorLayout);
        dpnContenedorLayout.setHorizontalGroup(
            dpnContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        dpnContenedorLayout.setVerticalGroup(
            dpnContenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 425, Short.MAX_VALUE)
        );

        mnuArchivo.setText("Archivo");

        mnuActividades.setText("Actividades");
        mnuActividades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuActividadesActionPerformed(evt);
            }
        });
        mnuArchivo.add(mnuActividades);

        jMenuItem18.setText("Colegio");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem18);

        jMenuItem19.setText("Cargo");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem19);

        jMenuItem2.setText("Departamentos Hospital");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem2);

        jMenuItem3.setText("Especialidades");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem3);

        jMenuItem4.setText("Servicios");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem4);

        jMenuItem5.setText("Turnos");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem5);

        jMenuItem15.setText("Tarifario");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem15);

        jMenuItem20.setText("Personal");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem20);

        jMenuItem21.setText("Procedimientos");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem21);
        mnuArchivo.add(jSeparator1);

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/1465376012_doctor.png"))); // NOI18N
        jMenuItem1.setText("Médicos");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem1);
        mnuArchivo.add(jSeparator2);

        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/areas.png"))); // NOI18N
        jMenuItem6.setText("Usuarios");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem6);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir2.png"))); // NOI18N
        jMenuItem7.setText("Salir");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        mnuArchivo.add(jMenuItem7);

        jMenuBar1.add(mnuArchivo);

        mnuTransacciones.setText("Transacciones");

        jMenuItem12.setText("Programación Rol de Asistencia");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        mnuTransacciones.add(jMenuItem12);

        jMenuItem22.setText("Listado Turnos Médicos");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        mnuTransacciones.add(jMenuItem22);

        mnuMedica.setText("Programación Médica");
        mnuTransacciones.add(mnuMedica);

        mnuRetenes.setText("Programación de Retenes");
        mnuRetenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuRetenesActionPerformed(evt);
            }
        });
        mnuTransacciones.add(mnuRetenes);

        mnuPagos.setText("Estimado de Pagos");
        mnuPagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPagosActionPerformed(evt);
            }
        });
        mnuTransacciones.add(mnuPagos);

        jMenuBar1.add(mnuTransacciones);

        jMenu3.setText("Reportes");

        mnuProgramacionIndividual.setText("Programación Médica Individual");
        mnuProgramacionIndividual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuProgramacionIndividualActionPerformed(evt);
            }
        });
        jMenu3.add(mnuProgramacionIndividual);

        mnuResumenProgramacion.setText("Resumen Programación Médica");
        mnuResumenProgramacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuResumenProgramacionActionPerformed(evt);
            }
        });
        jMenu3.add(mnuResumenProgramacion);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Ayuda");
        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE)
            .addComponent(dpnContenedor)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(dpnContenedor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
       FrmDptoHospListado obj = new FrmDptoHospListado();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        FrmMedicoListado obj = new FrmMedicoListado();
       dpnContenedor.add(obj);
       obj.setSize(this.dpnContenedor.getSize());
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        FrmEspecialidadesListado obj = new FrmEspecialidadesListado();
        dpnContenedor.add(obj);
        obj.setVisible(true);
        
        Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        FrmServiciosListado obj = new FrmServiciosListado();
        dpnContenedor.add(obj);
        obj.setVisible(true);
        
        Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        FrmTurnosListado obj = new FrmTurnosListado();
        dpnContenedor.add(obj);
        obj.setVisible(true);
        
        Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        FrmProgramacionRolDeAsistencias obj = new FrmProgramacionRolDeAsistencias();
        dpnContenedor.add(obj);
//        obj.setSize(this.dpnContenedor.getSize());
        obj.setVisible(true);
   
        Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void mnuRetenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuRetenesActionPerformed
       FrmProgramacionRetenes obj = new FrmProgramacionRetenes();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_mnuRetenesActionPerformed

    private void mnuPagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPagosActionPerformed
        FrmEstimadoDePagos obj = new FrmEstimadoDePagos();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_mnuPagosActionPerformed

    private void mnuActividadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuActividadesActionPerformed
        FrmActividadesListado obj = new FrmActividadesListado();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_mnuActividadesActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        FrmTarifarioListado obj = new FrmTarifarioListado();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        FrmColegioListado obj = new FrmColegioListado();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        FrmCargoListado obj = new FrmCargoListado();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
       FrmPersonalListado obj = new FrmPersonalListado();
       dpnContenedor.add(obj);
       obj.setSize(this.dpnContenedor.getSize());
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed
        FrmProcedimientosListado obj = new FrmProcedimientosListado();
       dpnContenedor.add(obj);
       obj.setSize(this.dpnContenedor.getSize());
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem21ActionPerformed

    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
       FrmProgramacionAsistenciasListado obj = new FrmProgramacionAsistenciasListado();
       dpnContenedor.add(obj);
       obj.setVisible(true);
       
       Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem22ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        if (JOptionPane.showConfirmDialog(rootPane, "¿Desea realmente salir del sistema?",
                    "Salir del sistema", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            System.exit(0);
        } 
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void mnuProgramacionIndividualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuProgramacionIndividualActionPerformed
       FrmReporteProgramacionIndividual obj = new FrmReporteProgramacionIndividual(dpnContenedor);
        //obj.setTitle("Reporteeeeeeeeeeeeeeeee");
        obj.nombreArchivoReporte = "Formulario_v7.jasper";
        this.dpnContenedor.add(obj);
        obj.setVisible(true);  
    }//GEN-LAST:event_mnuProgramacionIndividualActionPerformed

    private void mnuResumenProgramacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuResumenProgramacionActionPerformed
        FrmReporteResumenProgramacion obj = new FrmReporteResumenProgramacion(dpnContenedor);
        //obj.setTitle("Reporteeeeeeeeeeeeeeeee");
        obj.nombreArchivoReporte = "resumen_programacion_medica.jasper";
        this.dpnContenedor.add(obj);
        obj.setVisible(true);  
    }//GEN-LAST:event_mnuResumenProgramacionActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        FrmUsuarioListado obj = new FrmUsuarioListado();
        dpnContenedor.add(obj);
        obj.setVisible(true);
        
        Dimension desktopSize = dpnContenedor.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        obj.show();
    }//GEN-LAST:event_jMenuItem6ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane dpnContenedor;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator13;
    private javax.swing.JToolBar.Separator jSeparator14;
    private javax.swing.JToolBar.Separator jSeparator15;
    private javax.swing.JToolBar.Separator jSeparator16;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblNombreUsuario;
    private javax.swing.JMenuItem mnuActividades;
    private javax.swing.JMenu mnuArchivo;
    private javax.swing.JMenuItem mnuMedica;
    private javax.swing.JMenuItem mnuPagos;
    private javax.swing.JMenuItem mnuProgramacionIndividual;
    private javax.swing.JMenuItem mnuResumenProgramacion;
    private javax.swing.JMenuItem mnuRetenes;
    private javax.swing.JMenu mnuTransacciones;
    // End of variables declaration//GEN-END:variables
}
