package presentacion;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import negocio.Procedimientos;
import util.Funciones;

public class FrmProcedimientosListado extends javax.swing.JInternalFrame {

    ResultSet resultado;
    public String operacion;
    public String grabadoCorrectamente = "no";

    public FrmProcedimientosListado() {
        initComponents();
        cargarLista();
        listar();
        cargarCamposFiltro();
    }

    private void cargarLista() {
        try {
            Procedimientos objProcedimientos = new Procedimientos();
            resultado = objProcedimientos.listar();
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }

    private void listar() {
        try {
            String alineacion[] = {"C", "C", "I"};
            int ancho[] = {80, 80, 1900};
            Funciones.llenarTablaBusqueda(tblListado, resultado, ancho, alineacion, this.cboFiltrarPor.getSelectedItem().toString(), this.txtValorBuscado.getText());
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }

    private void cargarCamposFiltro() {
        String camposFiltro[] = new Procedimientos().obetenerCamposFiltro();
        cboFiltrarPor.removeAllItems();
        for (int i = 0; i < camposFiltro.length; i++) {
            String campo = camposFiltro[i];
            this.cboFiltrarPor.addItem(campo);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbOpciones = new javax.swing.JToolBar();
        jLabel2 = new javax.swing.JLabel();
        cboFiltrarPor = new javax.swing.JComboBox<>();
        jSeparator13 = new javax.swing.JToolBar.Separator();
        jLabel3 = new javax.swing.JLabel();
        txtValorBuscado = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblListado = new javax.swing.JTable(){
            public boolean isCellEditable(int fila, int columna){
                return false;
            }
        };
        jToolBar1 = new javax.swing.JToolBar();
        btnAgregar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnRefrescar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        tbOpciones.setFloatable(false);
        tbOpciones.setRollover(true);

        jLabel2.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        jLabel2.setText("   FILTRAR POR:  ");
        tbOpciones.add(jLabel2);

        cboFiltrarPor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboFiltrarPor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFiltrarPorActionPerformed(evt);
            }
        });
        tbOpciones.add(cboFiltrarPor);
        tbOpciones.add(jSeparator13);

        jLabel3.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        jLabel3.setText("VALOR BUSCADO:");
        tbOpciones.add(jLabel3);

        txtValorBuscado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtValorBuscadoActionPerformed(evt);
            }
        });
        txtValorBuscado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtValorBuscadoKeyReleased(evt);
            }
        });
        tbOpciones.add(txtValorBuscado);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Brush Script MT", 1, 48)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setText("Listado de Procedimientos");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(156, 156, 156)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblListado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblListado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblListadoMouseClicked(evt);
            }
        });
        tblListado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblListadoKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblListado);

        jToolBar1.setFloatable(false);
        jToolBar1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jToolBar1.setRollover(true);

        btnAgregar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/AGREGAR.png"))); // NOI18N
        btnAgregar.setFocusable(false);
        btnAgregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAgregar);

        btnEditar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/EDITAR.png"))); // NOI18N
        btnEditar.setFocusable(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnEditar);

        btnEliminar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ELIMINAR.png"))); // NOI18N
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnEliminar);

        btnRefrescar.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnRefrescar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ACTUALIZAR.png"))); // NOI18N
        btnRefrescar.setFocusable(false);
        btnRefrescar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefrescar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnRefrescar);

        btnSalir.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/SALIR.png"))); // NOI18N
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnSalir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tbOpciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tbOpciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboFiltrarPorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboFiltrarPorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboFiltrarPorActionPerformed

    private void txtValorBuscadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtValorBuscadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtValorBuscadoActionPerformed

    private void txtValorBuscadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorBuscadoKeyReleased
        listar();
    }//GEN-LAST:event_txtValorBuscadoKeyReleased

    private void tblListadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblListadoMouseClicked
        if (evt.getClickCount() == 2) {
            btnEditar.doClick();
        }
    }//GEN-LAST:event_tblListadoMouseClicked

    private void tblListadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblListadoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            this.btnEliminar.doClick();
        } else if (evt.getKeyCode() == KeyEvent.VK_F5) {
            this.btnRefrescar.doClick();
        } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.btnSalir.doClick();
        }
    }//GEN-LAST:event_tblListadoKeyPressed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        FrmProcedimientosAE objAE = new FrmProcedimientosAE(null, true);
        objAE.setTitle("Agregar datos de Procedimiento");
        objAE.operacion = "AGREGAR";
        objAE.setVisible(true);

        if (objAE.grabadoCorrectamente.equalsIgnoreCase("si")) {
            this.cargarLista();
            this.listar();
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        FrmProcedimientosAE objAgregarEditar = new FrmProcedimientosAE(null, true);
        objAgregarEditar.setTitle("Editar datos del Procedimiento");
        objAgregarEditar.operacion = "EDITAR";
        //Capturar el codigo de la lista para editar
        int filaSeleccionada = this.tblListado.getSelectedRow();
        if (filaSeleccionada < 0) {
            //quiere decir que no se ha seleccionado una fila
            Funciones.mensajeError("Debe seleccionar una fila", "¡VERIFICAR!");
            return;
        }

        int codigoEspecialidad = Integer.parseInt(this.tblListado.getValueAt(filaSeleccionada, 0).toString());
        objAgregarEditar.leerDatos(codigoEspecialidad);
        objAgregarEditar.setVisible(true);

        if (objAgregarEditar.grabadoCorrectamente.equalsIgnoreCase("si")) {
            //El Frm agregar/editar ha grabado correctamente
            this.cargarLista();
            this.listar();
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        int filaSeleccionada = this.tblListado.getSelectedRow();
        if (filaSeleccionada < 0) {
            //quiere decir que no se ha seleccionado una fila
            Funciones.mensajeError("Debe seleccionar una fila", "¡VERIFICAR!");
            return;
        }

        int idPersonal = Integer.parseInt(this.tblListado.getValueAt(filaSeleccionada, 0).toString());

        //Funciones.mensajeInformacion(String.valueOf(codigoArticulo), "Articulo eliminado");
        int respuestaUsr = Funciones.mensajeConfirmacion("Desea ELIMINAR el Procedimiento seleccionado", "¡CONFIRMAR!");
        if (respuestaUsr == 1) {
            return; //Se detiene el programa porque no elimina nada
        }

        try {
            Procedimientos obj = new Procedimientos();
            boolean resultado = obj.eliminar(idPersonal);
            if (resultado == true) {  //Si ha eliminado el registro correctamente entonces:
                cargarLista();
                listar();

            }
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnRefrescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarActionPerformed
        listar();
    }//GEN-LAST:event_btnRefrescarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        cboFiltrarPor.setPreferredSize(new Dimension(130, 25));
        this.txtValorBuscado.setPreferredSize(new Dimension(200, 25));

        tbOpciones.add(this.cboFiltrarPor, 1);
        tbOpciones.add(txtValorBuscado, 4);
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnRefrescar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cboFiltrarPor;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    public javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator13;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar tbOpciones;
    private javax.swing.JTable tblListado;
    private javax.swing.JTextField txtValorBuscado;
    // End of variables declaration//GEN-END:variables
}
