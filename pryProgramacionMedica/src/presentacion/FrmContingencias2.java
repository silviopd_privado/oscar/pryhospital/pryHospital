package presentacion;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import negocio.DetalleProgramacionMedica;
import negocio.Observaciones;
import negocio.ProgramacionMedicaAsistencias;
import util.Funciones;

public class FrmContingencias2 extends javax.swing.JDialog {
    
    public int accion = 0;
    public int mes = 0;
    public int CantidadDias = 0;
    public String year;
    public int year2;
    public int mes2;
    int total=0;
    
    boolean resultadoMedicoActual = false;
    boolean resultadoMedicoIntercambio = false;
    boolean resultadoObservaciones =false;    
    
    public FrmContingencias2(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        txtObservacion.setLineWrap (true);
        txtObservacion.setWrapStyleWord(true);
        dtFecha.setVisible(false);
        dtFechaCambio.setVisible(false);
        lblIdProgramacionActual.setVisible(false);
        lblIdProgramacionIntercambio.setVisible(false);
        //cargarTabla();
        //leerDatos(id, anio, mess);
    }
    
    private boolean validarDatos(){
        if(lblIdProgramacionIntercambio.getText().isEmpty()){
            Funciones.mensajeAdvertencia("Debe seleccionar un MEDICO de Intercambio", "¡VERIFIQUE!");
            this.btnBuscarMedicoCambio.doClick();
            return false;
        }else if(txtObservacion.getText().isEmpty()){
            Funciones.mensajeAdvertencia("Debe escribir las observaciones correspondientes", "¡VERIFIQUE!");
            this.txtObservacion.requestFocus();
            return false;
        }
        return true;
    }
    
    private void cargarFecha(int año,int mes) {
        GregorianCalendar dat=new GregorianCalendar(año, mes, 9);
        Calendar cal = new GregorianCalendar(año, mes-1, 9);
        dtFecha.setCalendar(cal);
        
    year2=dtFecha.getCalendar().getWeekYear();
    mes2=dtFecha.getCalendar().get(Calendar.MONTH);
        
    }
    
    public void leerDatos(int idMedico,int anio,int mess,JTable tb, int idPm) {
        cargarFecha(anio, mess);
        ResultSet resultado=null;
        
        try {
            switch (mess) {
        
                        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                                resultado = new ProgramacionMedicaAsistencias().leerDatosContingencias31(idMedico, idPm);
                                break;
                        case 4: case 6: case 9: case 11:
                                resultado = new ProgramacionMedicaAsistencias().leerDatosContingencias30(idMedico, idPm);
                                break;
                        case 2:                            
                                if ((anio % 4 == 0) && ((anio % 100 != 0) || (anio % 400 == 0))) {
                                resultado = new ProgramacionMedicaAsistencias().leerDatosContingencias29(idMedico, idPm);
                    }else{
                                    resultado = new ProgramacionMedicaAsistencias().leerDatosContingencias28(idMedico, idPm);
                                }
                                break;
                }
         cargarTabla(resultado,tb);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }
    
    public void cargarTabla(ResultSet resultado,JTable tbl){
    DefaultTableModel dtm = (DefaultTableModel) tbl.getModel();

        dtm.setColumnCount(0);
        dtm.setRowCount(0);
        if (tbl.getRowCount() <= 0) {
            dtm.addRow(new Object[]{"1", "2", ""});
        }
        Calendar cal = new GregorianCalendar(dtFecha.getDate().getYear(), dtFecha.getDate().getMonth(), dtFecha.getDate().getDay());
        cal.set(dtFecha.getDate().getYear(), dtFecha.getDate().getMonth(), dtFecha.getDate().getDay());

        java.sql.Date fechainicial = new java.sql.Date(this.dtFecha.getDate().getTime());
        java.util.Date fecha2 = new java.util.Date(this.dtFecha.getDate().getTime());

        String año = fecha2.toString();
        java.util.Date dat;
        dat = (dtFecha.getDate());


        String ultimoDD = Integer.toString(dtFecha.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH));
        String dia = Integer.toString(dtFecha.getCalendar().get(Calendar.DAY_OF_MONTH));
        //mes2 = Integer.toString(dtFecha.getCalendar().get(Calendar.MONTH) + 1);
        year = Integer.toString(dtFecha.getCalendar().get(Calendar.YEAR));
       // String fecha = (year + "-" + mes2 + "-" + dia);
        //System.out.println(fecha);
        dtm.addColumn("CÓDIGO");
        dtm.addColumn("ACTIVIDAD PROGRAMADA");
        dtm.addColumn("N. HORAS");
        for (int i = 1; i <= Integer.parseInt(ultimoDD); i++) {
            dtm.addColumn(" " + i);

        }


        Calendar cal2 = Calendar.getInstance();
        cal2.set(dtFecha.getCalendar().get(Calendar.YEAR), dtFecha.getCalendar().get(Calendar.MONTH), dtFecha.getCalendar().getActualMinimum(Calendar.DAY_OF_MONTH));

        String[] datos = new String[Integer.parseInt(ultimoDD) + 3];
        for (int i = 0; i < datos.length; i++) {
            datos[i] = null;
        }
        int a = 3;
        datos[0] = "";
        datos[1] = " ";
        datos[2] = "  ";
        String diaIngles = "";
        for (int i = 1; i <= Integer.parseInt(ultimoDD); i++) {
            diaIngles = cal2.getTime().toString().substring(0, 3);
            if (diaIngles.equalsIgnoreCase("Mon")) {
                datos[a] = "Lu";
            } else if (diaIngles.equalsIgnoreCase("Tue")) {
                datos[a] = "Ma";
            } else if (diaIngles.equalsIgnoreCase("Wed")) {
                datos[a] = "Mi";
            } else if (diaIngles.equalsIgnoreCase("thu")) {
                datos[a] = "Ju";
            } else if (diaIngles.equalsIgnoreCase("Fri")) {
                datos[a] = "Vi";
            } else if (diaIngles.equalsIgnoreCase("Sat")) {
                datos[a] = "Sa";
            } else {
                datos[a] = "Do";
            }
            cal2.add(Calendar.DAY_OF_MONTH, 1);
            if (a - 2 == Integer.parseInt(ultimoDD)) {
                break;
            }
            a++;
        }
        for (int i = 0; i < Integer.parseInt(ultimoDD) + 3; i++) {
            dtm.setValueAt(datos[i], 0, i);
        }
        if (Integer.parseInt(ultimoDD) + 3 == 34) {
            try {
                int ancho[] = {0, 190, 70, 30, 30, 30, 30, 30, 30, 30,
                               30,30,30,30,30,30,30,30,30,30,
                               30,30,30,30,30,30,30,30,30,30,
                               30,30,30,30};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D","D","D","D"};
                Funciones.llenarTablaPRG(tbl, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
            CantidadDias=34;
        }else{
            if (Integer.parseInt(ultimoDD) + 3 == 33) {
            try {
                int ancho[] = {0, 190, 80, 32, 32, 32, 32, 32, 32, 32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32,32};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D","D","D"};
                Funciones.llenarTablaPRG(tbl, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
//            llenarTablaActividades(ultimoDD+3,tblRolDeAsistencias);
            CantidadDias=33;
        }else{
                if (Integer.parseInt(ultimoDD) + 3 == 32) {
            try {
                int ancho[] = {0, 190, 80, 32, 32, 32, 32, 32, 32, 32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D","D"};
                Funciones.llenarTablaPRG(tbl, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
            CantidadDias=32;
        }else{
                    try {
                int ancho[] = {0, 190, 80, 34, 34, 34, 34, 34, 34, 34,
                               34,34,34,34,34,34,34,34,34,34,
                               34,34,34,34,34,34,34,34,34,34,
                               34};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D"};
                Funciones.llenarTablaPRG(tbl, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
                    CantidadDias=31;
                    
                }
            } 
        }

        tbl.getColumnModel().getColumn(0).setMaxWidth(0);
tbl.getColumnModel().getColumn(0).setMinWidth(0);
tbl.getColumnModel().getColumn(0).setPreferredWidth(0);

        try {
            int nroColumnas = resultado.getMetaData().getColumnCount();
        
        Object datoss[] = new Object[nroColumnas];
        while (resultado.next()){
            for(int i=0; i<nroColumnas;i++){
                datoss[i] = resultado.getObject(i+1);
             
            }
            dtm.addRow(datoss);
        }

        } catch (Exception e) {
        }

        tbl.setModel(dtm);

}
    
    public void ListarDiasMes(int año,int mes){
//    GregorianCalendar fecha=new GregorianCalendar();
//    Calendar fecha2=new GregorianCalendar(año, mes, 20);
//    fecha.set(año,mes, 1);
//    System.out.println(fecha);
//    System.out.println("dia: "+fecha.getTime()+"\nasdasd"+fecha.getCalendarType()+"\nasds"+fecha.toString());
//    System.out.println("nuevo: "+fecha2.getCalendarType());
//    System.out.println("nuevo2: "+fecha2.getTime());
        Calendar cal = new GregorianCalendar(dtFecha.getDate().getYear(), dtFecha.getDate().getMonth(), dtFecha.getDate().getDay());
        cal.set(dtFecha.getDate().getYear(), dtFecha.getDate().getMonth(), dtFecha.getDate().getDay());

        java.sql.Date fechainicial = new java.sql.Date(this.dtFecha.getDate().getTime());
        java.util.Date fecha2 = new java.util.Date(this.dtFecha.getDate().getTime());

//        String año = fecha2.toString();
//        java.util.Date dat;
//        dat = (txtDesde.getDate());
//
//
//        String ultimoDD = Integer.toString(txtDesde.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH));
//        String dia = Integer.toString(txtDesde.getCalendar().get(Calendar.DAY_OF_MONTH));
//        mes2 = Integer.toString(txtDesde.getCalendar().get(Calendar.MONTH) + 1);
//        year = Integer.toString(txtDesde.getCalendar().get(Calendar.YEAR));
//        
//        String fecha = (year + "-" + mes + "-" + dia);
//
//        dtm.addColumn("CÓDIGO");
//        dtm.addColumn("ACTIVIDAD PROGRAMADA");
//        dtm.addColumn("N. HORAS");
//        for (int i = 1; i <= Integer.parseInt(ultimoDD); i++) {
//            dtm.addColumn(" " + i);
//
//        }


//        java.sql.Date fechainicial = new java.sql.Date(this.txtDesde.getDate().getTime());
//        java.util.Date fecha2 = new java.util.Date(this.txtDesde.getDate().getTime());
//
//        String año = fecha2.toString();
//        java.util.Date dat;
//        dat = (txtDesde.getDate());
//
//
//        String ultimoDD = Integer.toString(txtDesde.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH));
//        String dia = Integer.toString(txtDesde.getCalendar().get(Calendar.DAY_OF_MONTH));
//        mes2 = Integer.toString(txtDesde.getCalendar().get(Calendar.MONTH) + 1);
//        year = Integer.toString(txtDesde.getCalendar().get(Calendar.YEAR));
//        
//        String fecha = (year + "-" + mes + "-" + dia);
}



private void grabarContingenciaMedicoActual(){
//     int respuesta = Funciones.mensajeConfirmacion("Desea grabar la Contingencia", "Confirme");
//        if (respuesta == 1){            
//            return;
//        }
            int IdprogramacionActual = Integer.parseInt(lblIdProgramacionActual.getText());  
        try {
         ProgramacionMedicaAsistencias obj = new ProgramacionMedicaAsistencias();
              
         ArrayList<DetalleProgramacionMedica> listaDetalleProgramacion=new ArrayList<DetalleProgramacionMedica>();
        for (int i = 1; i < this.tblMedicoActual.getRowCount(); i++) { 
                int codigoActividad=Integer.parseInt(tblMedicoActual.getValueAt(i, 0).toString());
                int numHoraXmes=Integer.parseInt(tblMedicoActual.getValueAt(i, 2).toString());
                int dia_1=Integer.parseInt(tblMedicoActual.getValueAt(i, 3).toString());
                int dia_2=Integer.parseInt(tblMedicoActual.getValueAt(i, 4).toString());
                int dia_3=Integer.parseInt(tblMedicoActual.getValueAt(i, 5).toString());
                int dia_4=Integer.parseInt(tblMedicoActual.getValueAt(i, 6).toString());
                int dia_5=Integer.parseInt(tblMedicoActual.getValueAt(i, 7).toString());
                int dia_6=Integer.parseInt(tblMedicoActual.getValueAt(i, 8).toString());
                int dia_7=Integer.parseInt(tblMedicoActual.getValueAt(i, 9).toString());
                int dia_8=Integer.parseInt(tblMedicoActual.getValueAt(i, 10).toString());
                int dia_9=Integer.parseInt(tblMedicoActual.getValueAt(i, 11).toString());
                int dia_10=Integer.parseInt(tblMedicoActual.getValueAt(i, 12).toString());
                int dia_11=Integer.parseInt(tblMedicoActual.getValueAt(i, 13).toString());
                int dia_12=Integer.parseInt(tblMedicoActual.getValueAt(i, 14).toString());
                int dia_13=Integer.parseInt(tblMedicoActual.getValueAt(i, 15).toString());
                int dia_14=Integer.parseInt(tblMedicoActual.getValueAt(i, 16).toString());
                int dia_15=Integer.parseInt(tblMedicoActual.getValueAt(i, 17).toString());
                int dia_16=Integer.parseInt(tblMedicoActual.getValueAt(i, 18).toString());
                int dia_17=Integer.parseInt(tblMedicoActual.getValueAt(i, 19).toString());
                int dia_18=Integer.parseInt(tblMedicoActual.getValueAt(i, 20).toString());
                int dia_19=Integer.parseInt(tblMedicoActual.getValueAt(i, 21).toString());
                int dia_20=Integer.parseInt(tblMedicoActual.getValueAt(i, 22).toString());
                int dia_21=Integer.parseInt(tblMedicoActual.getValueAt(i, 23).toString());
                int dia_22=Integer.parseInt(tblMedicoActual.getValueAt(i, 24).toString());
                int dia_23=Integer.parseInt(tblMedicoActual.getValueAt(i, 25).toString());
                int dia_24=Integer.parseInt(tblMedicoActual.getValueAt(i, 26).toString());
                int dia_25=Integer.parseInt(tblMedicoActual.getValueAt(i, 27).toString());
                int dia_26=Integer.parseInt(tblMedicoActual.getValueAt(i, 28).toString());
                int dia_27=Integer.parseInt(tblMedicoActual.getValueAt(i, 29).toString());
                int dia_28=Integer.parseInt(tblMedicoActual.getValueAt(i, 30).toString());
                int dia_29;
                if (tblMedicoActual.getColumnCount()==32) {
                    dia_29=Integer.parseInt(tblMedicoActual.getValueAt(i, 31).toString());
                }else{
                    dia_29=0;
                }
                int dia_30;
                if (tblMedicoActual.getColumnCount()==33) {
                    dia_30=Integer.parseInt(tblMedicoActual.getValueAt(i, 32).toString());
                }else{
                    dia_30=0;
                }
                int dia_31;
                
                if (tblMedicoActual.getColumnCount()==34) {
                    dia_31=Integer.parseInt(tblMedicoActual.getValueAt(i, 33).toString());
                }else{
                    dia_31=0;
                }
                
                 DetalleProgramacionMedica objMedicoActual=new DetalleProgramacionMedica();
                 
                objMedicoActual.setId_actividad(codigoActividad);
                objMedicoActual.setNro_horas_totales(numHoraXmes);
                objMedicoActual.setDia_1(dia_1);
                objMedicoActual.setDia_2(dia_2);
                objMedicoActual.setDia_3(dia_3);
                objMedicoActual.setDia_4(dia_4);
                objMedicoActual.setDia_5(dia_5);
                objMedicoActual.setDia_6(dia_6);
                objMedicoActual.setDia_7(dia_7);
                objMedicoActual.setDia_8(dia_8);
                objMedicoActual.setDia_9(dia_9);
                objMedicoActual.setDia_10(dia_10);
                objMedicoActual.setDia_11(dia_11);
                objMedicoActual.setDia_12(dia_12);
                objMedicoActual.setDia_13(dia_13);
                objMedicoActual.setDia_14(dia_14);
                objMedicoActual.setDia_15(dia_15);
                objMedicoActual.setDia_16(dia_16);
                objMedicoActual.setDia_17(dia_17);
                objMedicoActual.setDia_18(dia_18);
                objMedicoActual.setDia_19(dia_19);
                objMedicoActual.setDia_20(dia_20);
                objMedicoActual.setDia_21(dia_21);
                objMedicoActual.setDia_22(dia_22);
                objMedicoActual.setDia_23(dia_23);
                objMedicoActual.setDia_24(dia_24);
                objMedicoActual.setDia_25(dia_25);
                objMedicoActual.setDia_26(dia_26);
                objMedicoActual.setDia_27(dia_27);
                objMedicoActual.setDia_28(dia_28);
                objMedicoActual.setDia_29(dia_29);
                objMedicoActual.setDia_30(dia_30);
                objMedicoActual.setDia_31(dia_31);
                
                listaDetalleProgramacion.add(objMedicoActual);
                obj.setDetalle_programacion_medica(listaDetalleProgramacion);
        }
        obj.setId_programacion_medica(IdprogramacionActual);
        resultadoMedicoActual = obj.editarContingenciasMedicoActual();
         
//        if(resultadoMedicoActual == true){
//            Funciones.mensajeInformacion("Grabado Correctamente", "Detalle Programacion Medica");
//                this.dispose();
//        }
       
    } catch (Exception e) {
          Funciones.mensajeError(e.getMessage(), "¡ERROR!");
    }
        
       
        
}

private void grabarContingenciaMedicoIntercambio(){
//     int respuesta = Funciones.mensajeConfirmacion("Desea grabar la Contingencia", "Confirme");
//        if (respuesta == 1){            
//            return;
//        }           
            
            int IdprogramacionIntercambio = Integer.parseInt(lblIdProgramacionIntercambio.getText());  
        try {
         ProgramacionMedicaAsistencias obj = new ProgramacionMedicaAsistencias();
              
         ArrayList<DetalleProgramacionMedica> listaDetalleProgramacion=new ArrayList<DetalleProgramacionMedica>();
        for (int i = 1; i < this.tblMedicoIntercambio.getRowCount(); i++) { 
                int codigoActividad=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 0).toString());
                int numHoraXmes=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 2).toString());
                int dia_1=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 3).toString());
                int dia_2=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 4).toString());
                int dia_3=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 5).toString());
                int dia_4=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 6).toString());
                int dia_5=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 7).toString());
                int dia_6=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 8).toString());
                int dia_7=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 9).toString());
                int dia_8=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 10).toString());
                int dia_9=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 11).toString());
                int dia_10=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 12).toString());
                int dia_11=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 13).toString());
                int dia_12=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 14).toString());
                int dia_13=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 15).toString());
                int dia_14=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 16).toString());
                int dia_15=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 17).toString());
                int dia_16=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 18).toString());
                int dia_17=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 19).toString());
                int dia_18=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 20).toString());
                int dia_19=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 21).toString());
                int dia_20=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 22).toString());
                int dia_21=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 23).toString());
                int dia_22=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 24).toString());
                int dia_23=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 25).toString());
                int dia_24=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 26).toString());
                int dia_25=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 27).toString());
                int dia_26=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 28).toString());
                int dia_27=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 29).toString());
                int dia_28=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 30).toString());
                int dia_29;
                if (tblMedicoIntercambio.getColumnCount()==32) {
                    dia_29=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 31).toString());
                }else{
                    dia_29=0;
                }
                int dia_30;
                if (tblMedicoIntercambio.getColumnCount()==33) {
                    dia_30=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 32).toString());
                }else{
                    dia_30=0;
                }
                int dia_31;
                
                if (tblMedicoIntercambio.getColumnCount()==34) {
                    dia_31=Integer.parseInt(tblMedicoIntercambio.getValueAt(i, 33).toString());
                }else{
                    dia_31=0;
                }
                
                 DetalleProgramacionMedica objMedicoIntercambio=new DetalleProgramacionMedica();
                 
                objMedicoIntercambio.setId_actividad(codigoActividad);
                objMedicoIntercambio.setNro_horas_totales(numHoraXmes);
                objMedicoIntercambio.setDia_1(dia_1);
                objMedicoIntercambio.setDia_2(dia_2);
                objMedicoIntercambio.setDia_3(dia_3);
                objMedicoIntercambio.setDia_4(dia_4);
                objMedicoIntercambio.setDia_5(dia_5);
                objMedicoIntercambio.setDia_6(dia_6);
                objMedicoIntercambio.setDia_7(dia_7);
                objMedicoIntercambio.setDia_8(dia_8);
                objMedicoIntercambio.setDia_9(dia_9);
                objMedicoIntercambio.setDia_10(dia_10);
                objMedicoIntercambio.setDia_11(dia_11);
                objMedicoIntercambio.setDia_12(dia_12);
                objMedicoIntercambio.setDia_13(dia_13);
                objMedicoIntercambio.setDia_14(dia_14);
                objMedicoIntercambio.setDia_15(dia_15);
                objMedicoIntercambio.setDia_16(dia_16);
                objMedicoIntercambio.setDia_17(dia_17);
                objMedicoIntercambio.setDia_18(dia_18);
                objMedicoIntercambio.setDia_19(dia_19);
                objMedicoIntercambio.setDia_20(dia_20);
                objMedicoIntercambio.setDia_21(dia_21);
                objMedicoIntercambio.setDia_22(dia_22);
                objMedicoIntercambio.setDia_23(dia_23);
                objMedicoIntercambio.setDia_24(dia_24);
                objMedicoIntercambio.setDia_25(dia_25);
                objMedicoIntercambio.setDia_26(dia_26);
                objMedicoIntercambio.setDia_27(dia_27);
                objMedicoIntercambio.setDia_28(dia_28);
                objMedicoIntercambio.setDia_29(dia_29);
                objMedicoIntercambio.setDia_30(dia_30);
                objMedicoIntercambio.setDia_31(dia_31);
                
                listaDetalleProgramacion.add(objMedicoIntercambio);
                obj.setDetalle_programacion_medica(listaDetalleProgramacion);
        }
        obj.setId_programacion_medica(IdprogramacionIntercambio);
        resultadoMedicoIntercambio = obj.editarContingenciasMedicoIntercambio();
         
//        if(resultadoMedicoIntercambio == true){
//            Funciones.mensajeInformacion("Grabado Correctamente", "Detalle Programacion Medica");
//                this.dispose();
//        }
       
    } catch (Exception e) {
          Funciones.mensajeError(e.getMessage(), "¡ERROR!");
    }   
}

public void guardarObservacion(){
    try {
        int idProgramacion = Integer.parseInt(this.lblIdProgramacionActual.getText());
        String descripcion = this.txtObservacion.getText();
        
        Observaciones objObservaciones = new Observaciones();
        objObservaciones.setIdProgramacionMedica(idProgramacion);
        objObservaciones.setDescripcion(descripcion);
        
       resultadoObservaciones = objObservaciones.agregarObservaciones();
//        if(resultadoObservaciones == true){
//            Funciones.mensajeInformacion("Registro agregado con éxito", Funciones.NOMBRE_SOFTWARE);
//        }
    } catch (Exception e) {
        Funciones.mensajeError(e.getMessage(), "¡ERROR!");
    }
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        dtFecha = new com.toedter.calendar.JDateChooser();
        dtFechaCambio = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtnombres = new javax.swing.JTextField();
        txtAP_paterno = new javax.swing.JTextField();
        txtAP_materno = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        /*parte 1*/
        final JTextField field = new JTextField("usat");
        final DefaultCellEditor edit = new DefaultCellEditor(field);
        field.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.magenta));
        field.setForeground(Color.blue);
        /*parte 1*/
        tblMedicoActual = new javax.swing.JTable(){
            /*parte 2*/
            public boolean isCellEditable(int fila, int columna){
                if (columna == 0 || columna == 1 || columna == 2||fila==0){
                    return false;
                }
                return true;
            }

            public TableCellEditor getCellEditor(int row, int col) {
                if (col == 2){
                    field.setDocument(new util.ValidaNumeros());
                }else{
                    field.setDocument(new util.ValidaNumeros(util.ValidaNumeros.ACEPTA_DECIMAL));
                }
                edit.setClickCountToStart(2);
                field.addFocusListener(new FocusAdapter() {
                    public void focusLost(FocusEvent e) {
                        field.select(0,0);
                    }
                });
                return edit;
            }
            /*parte 2*/};
        lblTotalHoras = new javax.swing.JLabel();
        txtN_HorasTotal = new javax.swing.JTextField();
        lblIdProgramacionActual = new javax.swing.JTextField();
        lblMotivo = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtObservacion = new javax.swing.JTextArea();
        lblCarac = new javax.swing.JLabel();
        lblCarac2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        txtnombresCambio = new javax.swing.JTextField();
        txtAP_paternoCambio = new javax.swing.JTextField();
        txtAP_maternoCambio = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnBuscarMedicoCambio = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        /*parte 1*/
        final JTextField field2 = new JTextField("usat");
        final DefaultCellEditor edit2 = new DefaultCellEditor(field2);
        field2.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.magenta));
        field2.setForeground(Color.blue);
        /*parte 1*/
        tblMedicoIntercambio = new javax.swing.JTable(){
            /*parte 2*/
            public boolean isCellEditable(int fila, int columna){
                if (columna == 0 || columna == 1 || columna == 2||fila==0){
                    return false;
                }
                return true;
            }

            public TableCellEditor getCellEditor(int row, int col) {
                if (col == 2){
                    field2.setDocument(new util.ValidaNumeros());
                }else{
                    field2.setDocument(new util.ValidaNumeros(util.ValidaNumeros.ACEPTA_DECIMAL));
                }
                edit2.setClickCountToStart(2);
                field2.addFocusListener(new FocusAdapter() {
                    public void focusLost(FocusEvent e) {
                        field2.select(0,0);
                    }
                });
                return edit2;
            }
            /*parte 2*/};
        txtN_HorasTotal1 = new javax.swing.JTextField();
        lblTotalHoras1 = new javax.swing.JLabel();
        lblIdProgramacionIntercambio = new javax.swing.JTextField();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel4.setBackground(new java.awt.Color(70, 99, 138));

        jPanel1.setBackground(new java.awt.Color(247, 254, 255));

        jLabel4.setFont(new java.awt.Font("Brush Script MT", 1, 48)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 0, 0));
        jLabel4.setText("Contingencias");

        dtFechaCambio.setEnabled(false);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/hosp_las_mercedes - copia.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(dtFechaCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(380, 380, 380)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(dtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(72, 72, 72)
                .addComponent(jLabel3)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(dtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(dtFechaCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38))))
        );

        jPanel5.setBackground(new java.awt.Color(247, 254, 255));

        jPanel2.setBackground(new java.awt.Color(247, 254, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MEDICO ACTUAL", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        txtnombres.setEditable(false);

        txtAP_paterno.setEditable(false);

        txtAP_materno.setEditable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Ap. Paterno:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Ap. Materno:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Nombres:");

        tblMedicoActual.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        tblMedicoActual.setShowVerticalLines(false);
        tblMedicoActual.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tblMedicoActualPropertyChange(evt);
            }
        });
        tblMedicoActual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tblMedicoActualKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(tblMedicoActual);
        /*parte 3*/
        tblMedicoActual.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                field.setText("");
                field.requestFocus();
            }
        });

        field.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode()==10){
                    if (field.getText().isEmpty()){
                        evt.consume();
                    }
                }

                //char caracter = evt.getKeyChar();
                //if(((evt.getKeyCode() == 50) || (evt.getKeyChar() == 4)|| (evt.getKeyChar() == 6)|| (evt.getKeyChar()== 8) || evt.getKeyChar()==KeyEvent.VK_BACK_SPACE)){
                    //          System.out.println(evt.getKeyChar()+" <==valor nuevo SI tabla presionado");
                    //        }else{
                    //                  evt.setKeyChar((char) KeyEvent.VK_CLEAR);
                    //evt.consume();
                    //            System.out.println(evt.getKeyChar()+" <==valor nuevo NO tabla presionado");
                    //          }
            }
        });
        /*parte 3*/

        lblTotalHoras.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        lblTotalHoras.setText("N.Horas Total:");

        txtN_HorasTotal.setEditable(false);

        lblIdProgramacionActual.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnombres, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAP_paterno, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAP_materno, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(118, 118, 118)
                        .addComponent(lblIdProgramacionActual, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addComponent(lblTotalHoras)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtN_HorasTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtnombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtAP_paterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel5)
                        .addComponent(txtAP_materno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblIdProgramacionActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotalHoras)
                    .addComponent(txtN_HorasTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        lblMotivo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblMotivo.setForeground(new java.awt.Color(0, 153, 153));
        lblMotivo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMotivo.setText("Motivo del cambio de turno* "); // NOI18N

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtObservacion.setColumns(20);
        txtObservacion.setRows(5);
        txtObservacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtObservacionKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(txtObservacion);

        lblCarac.setText("El numero de caracteres es:");

        lblCarac2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblCarac2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCarac2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("*Max. 300 caracteres");

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(247, 254, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DEL MEDICO A CAMBIAR EL TURNO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        txtnombresCambio.setEditable(false);
        txtnombresCambio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombresCambioActionPerformed(evt);
            }
        });

        txtAP_paternoCambio.setEditable(false);
        txtAP_paternoCambio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAP_paternoCambioActionPerformed(evt);
            }
        });

        txtAP_maternoCambio.setEditable(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Ap. Paterno:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Ap. Materno:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Nombres:");

        btnBuscarMedicoCambio.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBuscarMedicoCambio.setText("Buscar Medico de Intercambio");
        btnBuscarMedicoCambio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarMedicoCambioActionPerformed(evt);
            }
        });

        tblMedicoIntercambio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        tblMedicoIntercambio.setShowVerticalLines(false);
        tblMedicoIntercambio.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tblMedicoIntercambioPropertyChange(evt);
            }
        });
        tblMedicoIntercambio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tblMedicoIntercambioKeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(tblMedicoIntercambio);
        /*parte 3*/
        tblMedicoActual.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                field2.setText("");
                field2.requestFocus();
            }
        });

        field2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode()==10){
                    if (field2.getText().isEmpty()){
                        evt.consume();
                    }
                }

                //char caracter = evt.getKeyChar();
                //if(((evt.getKeyCode() == 50) || (evt.getKeyChar() == 4)|| (evt.getKeyChar() == 6)|| (evt.getKeyChar()== 8) || evt.getKeyChar()==KeyEvent.VK_BACK_SPACE)){
                    //          System.out.println(evt.getKeyChar()+" <==valor nuevo SI tabla presionado");
                    //        }else{
                    //                  evt.setKeyChar((char) KeyEvent.VK_CLEAR);
                    //evt.consume();
                    //            System.out.println(evt.getKeyChar()+" <==valor nuevo NO tabla presionado");
                    //          }
            }
        });
        /*parte 3*/

        txtN_HorasTotal1.setEditable(false);

        lblTotalHoras1.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        lblTotalHoras1.setText("N.Horas Total:");

        lblIdProgramacionIntercambio.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnombresCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAP_paternoCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAP_maternoCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnBuscarMedicoCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(62, 62, 62)
                                .addComponent(lblTotalHoras1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtN_HorasTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(66, 66, 66)
                        .addComponent(lblIdProgramacionIntercambio, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 107, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtnombresCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtAP_paternoCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7)
                        .addComponent(jLabel8)
                        .addComponent(txtAP_maternoCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBuscarMedicoCambio)
                        .addComponent(lblIdProgramacionIntercambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotalHoras1)
                    .addComponent(txtN_HorasTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblCarac)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblCarac2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1115, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(lblMotivo)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lblMotivo, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(68, 68, 68)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(lblCarac, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCarac2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

        private void llenarTablaActividades(String cod,String Actividad,JTable tabla) {
        if (Actividad.equalsIgnoreCase("Capacitación23")) {
            Funciones.mensajeAdvertencia("SOLO SE PODRA INGRESAR 2 HORAS AL DIA", "ADVERTENCIA");
        }
    DefaultTableModel modelo = (DefaultTableModel) this.tblMedicoActual.getModel();    
    String datosVacios[]= new String[CantidadDias];
        datosVacios[0]=cod;
        datosVacios[1]=Actividad;
        int a=0;
        if (true) {
            for (int i = 2; i < CantidadDias; i++) {
                datosVacios[i]=String.valueOf(0);
            }
        }
        modelo.addRow(datosVacios);
        tabla.setModel(modelo);
    }
        
    private void tblMedicoActualPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tblMedicoActualPropertyChange

        if (evt.getPropertyName().equalsIgnoreCase("tableCellEditor")) {
            int fila = this.tblMedicoActual.getSelectedRow();
            int columna = this.tblMedicoActual.getSelectedColumn();
            int acomular=0;
            int valorActual=Integer.parseInt(tblMedicoActual.getValueAt(fila, columna).toString());
            int valorFinal=Integer.parseInt(tblMedicoActual.getValueAt(fila, columna).toString());
                        
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Capacitación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Capacitacion", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Interconsulta"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Interconsulta", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Telemedicina"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Telemedicina", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Auditoría"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Auditoría", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Comité"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Comité", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Referencias y Contrareferencias"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Referencias y Contrareferencias", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Supervisión"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Supervisión", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==12||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Emergencia"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 12 horas para Emergencia", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Información, Educación y Comunicación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Información, Educación y Comunicación", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Investigación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Investigación", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Junta Médica"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Junta Médica", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Procedimiento Diagnóstico Terapéutico"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Procedimiento Diagnóstico Terapéutico", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0||valorActual==4)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Visita Médica en Hospitalización"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2 o 4 horas para Visita Médica en Hospitalización", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==0||valorActual==4)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Consulta externa"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 4 horas para Consulta externa", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0||valorActual==4||valorActual==6)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Sala de Operaciones"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2, 4 o 6 horas para Sala de Operaciones", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0||valorActual==4||valorActual==6)&&tblMedicoActual.getValueAt(fila, 1).toString().equalsIgnoreCase("Centro Obstétrico"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2, 4 o 6 horas para Centro Obstétrico", "ADVERTENCIA");
                        tblMedicoActual.setValueAt(0, fila, columna);
                        return;
                    }
                    if (!(valorActual==2||valorActual==4||valorActual==6||valorActual==12||valorActual==0)) {
                        Funciones.mensajeError("No se admite valores difrentes a 0 2 4 8 ó 12", "Error");
                        tblMedicoActual.setValueAt(0, fila, columna);
                    }
            
         
            for (int i = 3; i < CantidadDias; i++) {
                acomular +=Integer.valueOf((this.tblMedicoActual.getValueAt(fila, i).toString()));
            }
           
            this.tblMedicoActual.setValueAt(acomular, fila, 2);
            total+=Integer.valueOf((this.tblMedicoActual.getValueAt(fila, columna).toString()));
            this.txtN_HorasTotal.setText(String.valueOf(total));
        }
    }//GEN-LAST:event_tblMedicoActualPropertyChange

    private void tblMedicoActualKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblMedicoActualKeyTyped
        int filaselec = tblMedicoActual.getSelectedRow();
        int columselec = tblMedicoActual.getSelectedColumn();
    }//GEN-LAST:event_tblMedicoActualKeyTyped

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if(this.validarDatos() == false){
                return; // Detiene el programa
             }
        
        grabarContingenciaMedicoActual();
        grabarContingenciaMedicoIntercambio();
        guardarObservacion();
        
        int respuesta = Funciones.mensajeConfirmacion("Desea grabar la Contingencia", "Confirme");
        if (respuesta == 1){            
            return;
        }    

        if(resultadoMedicoActual == true && resultadoMedicoIntercambio == true && resultadoObservaciones == true){
            Funciones.mensajeInformacion("Registro agregado con éxito", Funciones.NOMBRE_SOFTWARE);
            this.dispose();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtObservacionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtObservacionKeyTyped
        lblCarac2.setText(String.valueOf(txtObservacion.getText().length()));
        if (txtObservacion.getText().length()>=300) {
            Funciones.mensajeError("No se puede ingresar mas de 300 caracteres", "Error");
            evt.consume();
            return;
        }
    }//GEN-LAST:event_txtObservacionKeyTyped

    private void tblMedicoIntercambioPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tblMedicoIntercambioPropertyChange
       
        if (evt.getPropertyName().equalsIgnoreCase("tableCellEditor")) {
            int fila = this.tblMedicoIntercambio.getSelectedRow();
            int columna = this.tblMedicoIntercambio.getSelectedColumn();
            int acomular=0;
            int valorActual=Integer.parseInt(tblMedicoIntercambio.getValueAt(fila, columna).toString());
            int valorFinal=Integer.parseInt(tblMedicoIntercambio.getValueAt(fila, columna).toString());
                        
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Capacitación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Capacitacion", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Interconsulta"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Interconsulta", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Telemedicina"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Telemedicina", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Auditoría"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Auditoría", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Comité"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Comité", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Referencias y Contrareferencias"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Referencias y Contrareferencias", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Supervisión"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Supervisión", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==12||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Emergencia"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 12 horas para Emergencia", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Información, Educación y Comunicación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Información, Educación y Comunicación", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Investigación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Investigación", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Junta Médica"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Junta Médica", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Procedimiento Diagnóstico Terapéutico"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Procedimiento Diagnóstico Terapéutico", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0||valorActual==4)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Visita Médica en Hospitalización"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2 o 4 horas para Visita Médica en Hospitalización", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==0||valorActual==4)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Consulta externa"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 4 horas para Consulta externa", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0||valorActual==4||valorActual==6)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Sala de Operaciones"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2, 4 o 6 horas para Sala de Operaciones", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valorActual==2||valorActual==0||valorActual==4||valorActual==6)&&tblMedicoIntercambio.getValueAt(fila, 1).toString().equalsIgnoreCase("Centro Obstétrico"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2, 4 o 6 horas para Centro Obstétrico", "ADVERTENCIA");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                        return;
                    }
                    if (!(valorActual==2||valorActual==4||valorActual==6||valorActual==12||valorActual==0)) {
                        Funciones.mensajeError("No se admite valores difrentes a 0 2 4 8 ó 12", "Error");
                        tblMedicoIntercambio.setValueAt(0, fila, columna);
                    }
            
         
            for (int i = 3; i < CantidadDias; i++) {
                acomular +=Integer.valueOf((this.tblMedicoIntercambio.getValueAt(fila, i).toString()));
            }
           
            this.tblMedicoIntercambio.setValueAt(acomular, fila, 2);
            total+=Integer.valueOf((this.tblMedicoIntercambio.getValueAt(fila, columna).toString()));
            this.txtN_HorasTotal.setText(String.valueOf(total));
        }
    }//GEN-LAST:event_tblMedicoIntercambioPropertyChange

    private void tblMedicoIntercambioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblMedicoIntercambioKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_tblMedicoIntercambioKeyTyped

    private void btnBuscarMedicoCambioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarMedicoCambioActionPerformed
        FrmListadoMedicosCambio obj=new FrmListadoMedicosCambio(null, true);
        //obj.nombre=txtnombres.getText();
        //obj.setNombre(txtnombres.getText());
       
//        obj.apellido_pa=txtAP_paterno.getText();
//        obj.apellido_ma=txtAP_materno.getText();
        obj.lblNombresAntiguo.setText(txtnombres.getText());
        obj.lblAP_maternosAntiguo.setText(txtAP_materno.getText());
        obj.lblAP_paternosAntiguo.setText(txtAP_paterno.getText());
        obj.mes=mes2+1;
        obj.anioo=year2;
        obj.setVisible(true);
        int accioResultado = obj.accion;
        if (accioResultado == 0) {
            return;
        }
        
      
        leerDatos(Integer.parseInt(obj.lblIdMedico.getText()), year2, mes2+1,tblMedicoIntercambio,Integer.parseInt(obj.tblListado.getValueAt(obj.tblListado.getSelectedRow(), 0).toString()));
        this.txtnombresCambio.setText(obj.lblNombres.getText());
        this.txtAP_paternoCambio.setText(obj.lblAP_paternos.getText());
        this.txtAP_maternoCambio.setText(obj.lblAP_maternos.getText());
        this.lblIdProgramacionIntercambio.setText(obj.tblListado.getValueAt(obj.tblListado.getSelectedRow(), 0).toString());        
        
    }//GEN-LAST:event_btnBuscarMedicoCambioActionPerformed

    private void txtnombresCambioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombresCambioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombresCambioActionPerformed

    private void txtAP_paternoCambioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAP_paternoCambioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAP_paternoCambioActionPerformed


    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarMedicoCambio;
    private javax.swing.JButton btnGuardar;
    private com.toedter.calendar.JDateChooser dtFecha;
    private com.toedter.calendar.JDateChooser dtFechaCambio;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblCarac;
    private javax.swing.JLabel lblCarac2;
    public javax.swing.JTextField lblIdProgramacionActual;
    public javax.swing.JTextField lblIdProgramacionIntercambio;
    private javax.swing.JLabel lblMotivo;
    private javax.swing.JLabel lblTotalHoras;
    private javax.swing.JLabel lblTotalHoras1;
    public javax.swing.JTable tblMedicoActual;
    public javax.swing.JTable tblMedicoIntercambio;
    public javax.swing.JTextField txtAP_materno;
    public javax.swing.JTextField txtAP_maternoCambio;
    public javax.swing.JTextField txtAP_paterno;
    public javax.swing.JTextField txtAP_paternoCambio;
    private javax.swing.JTextField txtN_HorasTotal;
    private javax.swing.JTextField txtN_HorasTotal1;
    private javax.swing.JTextArea txtObservacion;
    public javax.swing.JTextField txtnombres;
    public javax.swing.JTextField txtnombresCambio;
    // End of variables declaration//GEN-END:variables
}
