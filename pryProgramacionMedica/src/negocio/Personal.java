package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Personal extends Conexion {

    private int idPersonal;
    private String dni;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String sexo;
    private String fechaNacimiento;
    private java.sql.Date fechaIngreso;
    private java.sql.Date fechaSalida;
    private String estado;
    private int idCargo;
    private int idTipoPersonal;
    private int idSituacionLaboral;

    public static ArrayList<Personal> listaPersonal = new ArrayList<Personal>();

    public int getIdPersonal() {
        return idPersonal;
    }

    public void setIdPersonal(int idPersonal) {
        this.idPersonal = idPersonal;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public int getIdTipoPersonal() {
        return idTipoPersonal;
    }

    public void setIdTipoPersonal(int idTipoPersonal) {
        this.idTipoPersonal = idTipoPersonal;
    }

    public int getIdSituacionLaboral() {
        return idSituacionLaboral;
    }

    public void setIdSituacionLaboral(int idSituacionLaboral) {
        this.idSituacionLaboral = idSituacionLaboral;
    }

    private void cargarDatosTabla(int id_cargo, int id_tipo_personal, int id_situacion_laboral) throws Exception {
        String sql = "SELECT \n"
                + "  personal.id_personal, \n"
                + "  personal.dni, \n"
                + "  personal.apellido_paterno, \n"
                + "  personal.apellido_materno, \n"
                + "  personal.nombres, \n"
                + "  personal.sexo, \n"
                + "  personal.fecha_nacimiento, \n"
                + "  cargo.descripcion as cargo, \n"
                + "  tipo_personal.descripcion as tipo_personal, \n"
                + "  situacion_laboral.descripcion as situacion_laboral\n"
                + "FROM \n"
                + "  public.personal, \n"
                + "  public.cargo, \n"
                + "  public.tipo_personal, \n"
                + "  public.situacion_laboral\n"
                + "WHERE \n"
                + "  cargo.id_cargo = personal.id_cargo AND\n"
                + "  tipo_personal.id_tipo_personal = personal.id_tipo_personal AND\n"
                + "  situacion_laboral.id_situacion_laboral = personal.id_situacion_laboral and "
                + "  public.cargo = ? and public.tipo_personal = ? and public.situacion_laboral = ? order by 1 ;";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        sp.setInt(1, id_cargo);
        sp.setInt(2, id_tipo_personal);
        sp.setInt(3, id_situacion_laboral);
        ResultSet resultado = ejecutarSqlSelectSP(sp);

        listaPersonal.clear();

        while (resultado.next()) {
            Personal objServicio = new Personal();
            objServicio.setIdPersonal(resultado.getInt("id_personal"));
            objServicio.setDni(resultado.getString("dni"));
//            objServicio.setApellidoMaterno(resultado.getString("apellido_materno"));
//            objServicio.setApellidoPaterno(resultado.getString("apellido_paterno"));
//            objServicio.setNombres(resultado.getString("nombres"));
//            objServicio.setIdCargo(resultado.getInt("id_cargo"));
            listaPersonal.add(objServicio);
        }
    }

    public void cargarCombo(JComboBox objComboBox, int id_cargo, int id_tipo_personal, int id_situacion_laboral) throws Exception {
        cargarDatosTabla(id_cargo, id_tipo_personal, id_situacion_laboral);
        objComboBox.removeAllItems();
        for (int i = 0; i < listaPersonal.size(); i++) {
            Personal item = listaPersonal.get(i);
            objComboBox.addItem(item.nombres);
        }
    }

    public ResultSet listar() throws Exception {
        String sql = "SELECT \n"
                + "  personal.id_personal, \n"
                + "  personal.dni, \n"
                + "  personal.apellido_paterno, \n"
                + "  personal.apellido_materno, \n"
                + "  personal.nombres, \n"
                + "  personal.sexo, \n"
                + "  personal.fecha_nacimiento, \n"
                + "  personal.estado, \n"
                + "  cargo.descripcion as cargo, \n"
                + "  tipo_personal.descripcion as tipo_personal, \n"
                + "  situacion_laboral.descripcion as situacion_laboral\n"
                + "FROM \n"
                + "  public.personal, \n"
                + "  public.cargo, \n"
                + "  public.tipo_personal, \n"
                + "  public.situacion_laboral\n"
                + "WHERE \n"
                + "  cargo.id_cargo = personal.id_cargo AND\n"
                + "  tipo_personal.id_tipo_personal = personal.id_tipo_personal AND\n"
                + "  situacion_laboral.id_situacion_laboral = personal.id_situacion_laboral\n"
                + "order by\n"
                + "	1;";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('personal') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigo = resultado.getInt("nc");
            this.setIdPersonal(nuevoCodigo);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO personal(id_personal, dni, apellido_paterno, apellido_materno, nombres, \n" +
            "            sexo, fecha_nacimiento, id_cargo, id_tipo_personal, id_situacion_laboral)\n" +
            "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdPersonal());
            spInsertar.setString(2, getDni());
            spInsertar.setString(3, getApellidoPaterno());
            spInsertar.setString(4, getApellidoMaterno());
            spInsertar.setString(5, getNombres());
            spInsertar.setString(6, getSexo());
            spInsertar.setString(7, getFechaNacimiento());
            spInsertar.setInt(8, getIdCargo());
            spInsertar.setInt(9, getIdTipoPersonal());
            spInsertar.setInt(10, getIdSituacionLaboral());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'personal' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Personal");
        }
        return true;
    }

    public String[] obtenerCamposFiltro() {
        String camposFiltro[] = {"DNI", "APELLIDO_PATERNO", "APELLIDO_MATERNO", "NOMBRES"};
        return camposFiltro;
    }

    public ResultSet leerDatos(int idPersonal) throws Exception {
        String sql = "Select * from personal where id_personal = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idPersonal);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE personal\n" +
            "   SET dni=?, apellido_paterno=?, apellido_materno=?, \n" +
            "       nombres=?, sexo=?, fecha_nacimiento=?, id_cargo=?, \n" +
            "       id_tipo_personal=?, id_situacion_laboral=?\n" +
            " WHERE id_personal=?;";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, getDni());
        sentencia.setString(2, getApellidoPaterno());
        sentencia.setString(3, getApellidoMaterno());
        sentencia.setString(4, getNombres());
        sentencia.setString(5, getSexo());
        sentencia.setString(6, getFechaNacimiento());
        sentencia.setInt(7, getIdCargo());
        sentencia.setInt(8, getIdTipoPersonal());
        sentencia.setInt(9, getIdSituacionLaboral());
        sentencia.setInt(10, getIdPersonal());

        this.ejecutarSql(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoServicio) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE from personal WHERE id_personal = ?;";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, codigoServicio);

        this.ejecutarSql(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        return true;
    }

}
