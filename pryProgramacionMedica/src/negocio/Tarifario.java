package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Tarifario extends Conexion {

    private int id_tarifario;
    private int id_colegio;
    private String descripcion;
    private String nivel;
    private String guardiadn;
    private String dia_laboral;
    private double tarifa;

    public ArrayList<Tarifario> listaTarifario = new ArrayList<Tarifario>();

    public int getId_tarifario() {
        return id_tarifario;
    }

    public void setId_tarifario(int id_tarifario) {
        this.id_tarifario = id_tarifario;
    }

    public int getId_colegio() {
        return id_colegio;
    }

    public void setId_colegio(int id_colegio) {
        this.id_colegio = id_colegio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getGuardiadn() {
        return guardiadn;
    }

    public void setGuardiadn(String guardiadn) {
        this.guardiadn = guardiadn;
    }

    public String getDia_laboral() {
        return dia_laboral;
    }

    public void setDia_laboral(String dia_laboral) {
        this.dia_laboral = dia_laboral;
    }

    public double getTarifa() {
        return tarifa;
    }

    public void setTarifa(double tarifa) {
        this.tarifa = tarifa;
    }

    private void cargarDatosTabla(int codigo_colegio) throws Exception {
        String sql = "select * from tarifario where id_colegio = ? order by 1";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        sp.setInt(1, codigo_colegio);
        ResultSet resultado = ejecutarSqlSelectSP(sp);

        listaTarifario.clear();

        while (resultado.next()) {
            Tarifario objTarifario = new Tarifario();
            objTarifario.setId_tarifario(resultado.getInt("id_especialidad"));
            objTarifario.setDescripcion(resultado.getString("descripcion"));
            listaTarifario.add(objTarifario);
        }
    }

    public void cargarCombo(JComboBox objComboBox, int codigoColegio) throws Exception {
        cargarDatosTabla(codigoColegio);
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTarifario.size(); i++) {
            Tarifario item = listaTarifario.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }

    public ResultSet listar() throws Exception {
        String sql = "SELECT \n"
                + "  tarifario.id_tarifario, \n"
                + "  colegio.descripcion as colegio, \n"
                + "  tarifario.descripcion as tarifario, \n"
                + "  tarifario.nivel, \n"
                + "  tarifario.guardiadn, \n"
                + "  tarifario.dia_laboral, \n"
                + "  tarifario.tarifa\n"
                + "FROM \n"
                + "  public.tarifario, \n"
                + "  public.colegio\n"
                + "WHERE \n"
                + "  colegio.id_colegio = tarifario.id_colegio\n"
                + "order by\n"
                + "	1;";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;
    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('tarifario') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigo = resultado.getInt("nc");
            this.setId_tarifario(nuevoCodigo);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO tarifario(\n"
                    + "            id_tarifario, id_colegio, descripcion, nivel, guardiadn, dia_laboral, \n"
                    + "            tarifa)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, \n"
                    + "            ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getId_tarifario());
            spInsertar.setInt(2, getId_colegio());
            spInsertar.setString(3, getDescripcion());
            spInsertar.setString(4, getNivel());
            spInsertar.setString(5, getGuardiadn());
            spInsertar.setString(6, getDia_laboral());
            spInsertar.setDouble(7, getTarifa());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'tarifario' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Tarifario");
        }
        return true;
    }

    public String[] obtenerCamposFiltro() {
        String camposFiltro[] = {"ID_TARIFARIO", "COLEGIO","TARIFARIO"};
        return camposFiltro;
    }

    public ResultSet leerDatos(int codigoEspecialidad) throws Exception {
        String sql = "Select * from tarifario where id_tarifario = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigoEspecialidad);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE tarifario\n"
                + "   SET id_colegio=?, descripcion=?, nivel=?, guardiadn=?, \n"
                + "       dia_laboral=?, tarifa=?\n"
                + " WHERE id_tarifario = ? ;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, this.getId_colegio());
        sentencia.setString(2, this.getDescripcion());
        sentencia.setString(3, this.getNivel());
        sentencia.setString(4, this.getGuardiadn());
        sentencia.setString(5, this.getDia_laboral());
        sentencia.setDouble(6, this.getTarifa());
        sentencia.setInt(7, this.getId_tarifario());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoEspecialidad) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE from tarifario WHERE id_tarifario = ?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, codigoEspecialidad);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

}
