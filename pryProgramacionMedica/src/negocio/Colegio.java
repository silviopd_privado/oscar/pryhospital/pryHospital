package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Colegio extends Conexion {

    private int idColegio;
    private String codigoColegio;
    private String descripcion;

    public static ArrayList<Colegio> listaColegio = new ArrayList<Colegio>();

    public int getIdColegio() {
        return idColegio;
    }

    public void setIdColegio(int idColegio) {
        this.idColegio = idColegio;
    }

    public String getCodigoColegio() {
        return codigoColegio;
    }

    public void setCodigoColegio(String codigoColegio) {
        this.codigoColegio = codigoColegio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void cargarDatosTabla() throws Exception {
        String sql = "select * from colegio order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaColegio.clear();
        while (resultado.next()) {
            Colegio objDptoHosp = new Colegio();
            objDptoHosp.setIdColegio(resultado.getInt("id_colegio"));
            objDptoHosp.setCodigoColegio(resultado.getString("codigo_colegio"));
            objDptoHosp.setDescripcion(resultado.getString("descripcion"));
            listaColegio.add(objDptoHosp);
        }
    }

    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaColegio.size(); i++) {
            Colegio item = listaColegio.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }

    public ResultSet listar() throws Exception {
        String sql = "Select * from colegio order by 2;";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);

        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }

    public String obetenerCamposFiltro() {
        String camposFiltro = "DESCRIPCION";
        return camposFiltro;
    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('colegio') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigoDpto = resultado.getInt("nc");
            this.setIdColegio(nuevoCodigoDpto);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO colegio(\n"
                    + "            id_colegio, codigo_colegio, descripcion)\n"
                    + "    VALUES (?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdColegio());
            spInsertar.setString(2, getCodigoColegio());
            spInsertar.setString(3, getDescripcion());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'colegio' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Colegio");
        }
        return true;
    }

    public ResultSet leerDatos(int idColegio) throws Exception {
        String sql = "Select * from colegio where id_colegio = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idColegio);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE colegio\n"
                + "   SET  codigo_colegio=?, descripcion=?\n"
                + " WHERE id_colegio=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getCodigoColegio());
        sentencia.setString(2, this.getDescripcion());
        sentencia.setInt(3, this.getIdColegio());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int idColegio) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE FROM public.colegio WHERE id_colegio=? ;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, idColegio);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

}
