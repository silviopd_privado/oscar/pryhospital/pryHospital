package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Actividad extends Conexion{
    
    private int id_actividades;
    private String cod_actividad;
    private String descripcion;
    
     public static ArrayList<Actividad> listaActividades = new ArrayList<Actividad>();

    public int getId_actividades() {
        return id_actividades;
    }

    public void setId_actividades(int id_actividades) {
        this.id_actividades = id_actividades;
    }

    public String getCod_actividad() {
        return cod_actividad;
    }

    public void setCod_actividad(String cod_actividad) {
        this.cod_actividad = cod_actividad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    public ResultSet listar()throws Exception{
            String sql = "SELECT 	id_actividades as id, cod_actividad as codigo, descripcion  FROM ACTIVIDADES;";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;
    }
    
    public ResultSet configurarCabeceraTabla() throws Exception {
        String sql = "select "
                + "	* "
                + "from( "
                + "	select "
                + "		0 as codigo, "
                + "		'':: varchar as cod(abrev), "
                + "		'':: varchar as Actividad, "
                + ")as tb_temp "
                + "where "
                + "	codigo > 0";
        return ejecutarSqlSelect(sql);
    }
    
    public void cargarDatosTabla() throws Exception {
        String sql = "SELECT id_actividades as id, cod_actividad as codigo, descripcion  FROM ACTIVIDADES order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaActividades.clear();
        while (resultado.next()) {
            Actividad objActividad = new Actividad();
            objActividad.setId_actividades(resultado.getInt("id_actividad"));
            objActividad.setCod_actividad(resultado.getString("cod_actividad"));
            objActividad.setDescripcion(resultado.getString("descripcion"));
            listaActividades.add(objActividad);
        }
    }
    
    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaActividades.size(); i++) {
            Actividad item = listaActividades.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }
    
    public String[] obetenerCamposFiltro() {
        String camposFiltro[] = {"CODIGO","DESCRIPCION"};
        return camposFiltro;
    }
    
       public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('actividades') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoIDActividades = resultado.getInt("nc");
            this.setId_actividades(nuevoIDActividades);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.actividades(id_actividades, cod_actividad, descripcion)\n" +
                "    VALUES (?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getId_actividades());
            spInsertar.setString(2, getCod_actividad());
            spInsertar.setString(3, getDescripcion());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'actividades' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla ACTIVIDADES");
        }
        return true;
    }

        public ResultSet leerDatos(int idActividades) throws Exception {
        String sql = "Select * from actividades where id_actividades = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idActividades);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
        
        public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.actividades SET cod_actividad=?, descripcion=? WHERE id_actividades=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getCod_actividad());
        sentencia.setString(2, this.getDescripcion());
        sentencia.setInt(3, this.getId_actividades());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }
        
        public boolean eliminar(int idActividades) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE FROM public.actividades WHERE id_actividades=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, idActividades);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }
    
    
}
