package negocio;

import accesoDatos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class TipoUsuario extends Conexion{
    
    private int codigoTipoUsuario;
    private String descripcion;
    
    public static ArrayList<TipoUsuario> listaTipoUsuario = new ArrayList<TipoUsuario>();

    public int getCodigoTipoUsuario() {
        return codigoTipoUsuario;
    }

    public void setCodigoTipoUsuario(int codigoTipoUsuario) {
        this.codigoTipoUsuario = codigoTipoUsuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public void cargarDatosTabla()throws Exception{
        String sql ="select * from tipo_usuario order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaTipoUsuario.clear();
        while(resultado.next()){
            TipoUsuario objTipoUsuario = new TipoUsuario();
            objTipoUsuario.setCodigoTipoUsuario(resultado.getInt("id_tipo_usuario"));
            objTipoUsuario.setDescripcion(resultado.getString("descripcion"));
            listaTipoUsuario.add(objTipoUsuario);
        }
    }
    
    public void llenarCombo(JComboBox objComboBox)throws Exception{
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTipoUsuario.size(); i++) {
            TipoUsuario item = listaTipoUsuario.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }
}
