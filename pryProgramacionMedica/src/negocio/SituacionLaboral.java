package negocio;

import accesoDatos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class SituacionLaboral extends Conexion {

    private int idSituacionLaboral;
    private String descripcion;
    public static ArrayList<SituacionLaboral> listaSituacionLaboral = new ArrayList<SituacionLaboral>();

    public int getIdSituacionLaboral() {
        return idSituacionLaboral;
    }

    public void setIdSituacionLaboral(int idSituacionLaboral) {
        this.idSituacionLaboral = idSituacionLaboral;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void cargarDatosTabla() throws Exception {
        String sql = "select * from situacion_laboral order by 1";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaSituacionLaboral.clear();
        while (resultado.next()) {
            SituacionLaboral objTipoEmpleado = new SituacionLaboral();
            objTipoEmpleado.setIdSituacionLaboral(resultado.getInt("id_situacion_laboral"));
            objTipoEmpleado.setDescripcion(resultado.getString("descripcion"));
            listaSituacionLaboral.add(objTipoEmpleado);
        }
    }

    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaSituacionLaboral.size(); i++) {
            SituacionLaboral item = listaSituacionLaboral.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }
}
