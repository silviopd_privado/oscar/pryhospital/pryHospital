package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class ProgramacionMedicaAsistencias extends Conexion{

private int id_programacion_medica;
private int id_medico;
private int id_servicio;
private int id_dpto;
private int mes;
private int año;
private int numhoras_total;

//public ArrayList<DetalleProgramacionMedica> listaDetalleProgramacionMedica = new ArrayList<DetalleProgramacionMedica>();

    public int getId_programacion_medica() {
        return id_programacion_medica;
    }

    public void setId_programacion_medica(int id_programacion_medica) {
        this.id_programacion_medica = id_programacion_medica;
    }

    public int getId_medico() {
        return id_medico;
    }

    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    public int getId_servicio() {
        return id_servicio;
    }

    public void setId_servicio(int id_servicio) {
        this.id_servicio = id_servicio;
    }

    public int getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(int id_dpto) {
        this.id_dpto = id_dpto;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public int getNumhoras_total() {
        return numhoras_total;
    }

    public void setNumhoras_total(int numhoras_total) {
        this.numhoras_total = numhoras_total;
    }
    
private ArrayList<DetalleProgramacionMedica> detalle_programacion_medica;
    

    public ArrayList<DetalleProgramacionMedica> getDetalle_programacion_medica() {
        return detalle_programacion_medica;
    }

    public void setDetalle_programacion_medica(ArrayList<DetalleProgramacionMedica> detalle_programacion_medica) {
        this.detalle_programacion_medica = detalle_programacion_medica;
    }


         public boolean agregar()throws Exception{
        String sql="select * from f_generar_correlativo('programacion_medica') as numero";
        ResultSet res=this.ejecutarSqlSelect(sql);
        if (res.next()) {
            int nroProgramacion_medica=res.getInt("numero");
            this.setId_programacion_medica(nroProgramacion_medica);
            
            Connection con=this.abrirConexion();
            con.setAutoCommit(false);
            sql="INSERT INTO programacion_medica(id_programacion_medica, id_medico, id_servicio, id_dpto,mes, \"año\", numhoras_total)"
                    + "VALUES (?, ?, ?, ?,?, ?, ?);";
            PreparedStatement spCompra=con.prepareStatement(sql);
            spCompra.setInt(1, this.getId_programacion_medica());
            spCompra.setInt(2, this.getId_medico());
            spCompra.setInt(3, this.getId_servicio());
            spCompra.setInt(4, this.getId_dpto());
            spCompra.setInt(5, this.getMes());
            spCompra.setInt(6, this.getAño());
            spCompra.setInt(7, this.getNumhoras_total());

            this.ejecutarSQLsp(spCompra, con);
                String sql2="select * from f_generar_correlativo('detalle_programacion_medica') as numero2";
                ResultSet res2=this.ejecutarSqlSelect(sql2);
                int idDetalle_prgra;
            if (res2.next()) {
                idDetalle_prgra=res2.getInt("numero2");
            }else{
                idDetalle_prgra=res2.getInt("numero2");
                }
            
            for (int i = 0; i < detalle_programacion_medica.size(); i++) {
//                System.out.println("cod detalle:"+detalle_programacion_medica.get(i).getId_detalle());
//                System.out.println("cantidad:"+detalleCompraArticulo.get(i).getCantidad());
//                System.out.println("precio:"+detalleCompraArticulo.get(i).getPrecio());
                sql="INSERT INTO detalle_programacion_medica( id_programacion_medica, id_actividad, dia_1, dia_2, dia_3, dia_4,"
                        + " dia_5,dia_6, dia_7, dia_8, dia_9, dia_10, dia_11, dia_12, dia_13, dia_14,dia_15, dia_16,"
                        + " dia_17, dia_18, dia_19, dia_20, dia_21, dia_22, dia_23, dia_24, dia_25, dia_26, dia_27,"
                        + " dia_28, dia_29, dia_30, dia_31, nro_horas_totales)"
                        + "VALUES (?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                PreparedStatement spDetailProgramacion=con.prepareStatement(sql);
                spDetailProgramacion.setInt(1, this.id_programacion_medica);
                spDetailProgramacion.setInt(2, detalle_programacion_medica.get(i).getId_actividad());
                spDetailProgramacion.setInt(3, detalle_programacion_medica.get(i).getDia_1());
                spDetailProgramacion.setInt(4, detalle_programacion_medica.get(i).getDia_2());
                spDetailProgramacion.setInt(5, detalle_programacion_medica.get(i).getDia_3());
                spDetailProgramacion.setInt(6, detalle_programacion_medica.get(i).getDia_4());
                spDetailProgramacion.setInt(7, detalle_programacion_medica.get(i).getDia_5());
                spDetailProgramacion.setInt(8, detalle_programacion_medica.get(i).getDia_6());
                spDetailProgramacion.setInt(9, detalle_programacion_medica.get(i).getDia_7());
                spDetailProgramacion.setInt(10, detalle_programacion_medica.get(i).getDia_8());
                spDetailProgramacion.setInt(11, detalle_programacion_medica.get(i).getDia_9());
                spDetailProgramacion.setInt(12, detalle_programacion_medica.get(i).getDia_10());
                spDetailProgramacion.setInt(13, detalle_programacion_medica.get(i).getDia_11());
                spDetailProgramacion.setInt(14, detalle_programacion_medica.get(i).getDia_12());
                spDetailProgramacion.setInt(15, detalle_programacion_medica.get(i).getDia_13());
                spDetailProgramacion.setInt(16, detalle_programacion_medica.get(i).getDia_14());
                spDetailProgramacion.setInt(17, detalle_programacion_medica.get(i).getDia_15());
                spDetailProgramacion.setInt(18, detalle_programacion_medica.get(i).getDia_16());
                spDetailProgramacion.setInt(19, detalle_programacion_medica.get(i).getDia_17());
                spDetailProgramacion.setInt(20, detalle_programacion_medica.get(i).getDia_18());
                spDetailProgramacion.setInt(21, detalle_programacion_medica.get(i).getDia_19());
                spDetailProgramacion.setInt(22, detalle_programacion_medica.get(i).getDia_20());
                spDetailProgramacion.setInt(23, detalle_programacion_medica.get(i).getDia_21());
                spDetailProgramacion.setInt(24, detalle_programacion_medica.get(i).getDia_22());
                spDetailProgramacion.setInt(25, detalle_programacion_medica.get(i).getDia_23());
                spDetailProgramacion.setInt(26, detalle_programacion_medica.get(i).getDia_24());
                spDetailProgramacion.setInt(27, detalle_programacion_medica.get(i).getDia_25());
                spDetailProgramacion.setInt(28, detalle_programacion_medica.get(i).getDia_26());
                spDetailProgramacion.setInt(29, detalle_programacion_medica.get(i).getDia_27());
                spDetailProgramacion.setInt(30, detalle_programacion_medica.get(i).getDia_28());
                spDetailProgramacion.setInt(31, detalle_programacion_medica.get(i).getDia_29());
                spDetailProgramacion.setInt(32, detalle_programacion_medica.get(i).getDia_30());
                spDetailProgramacion.setInt(33, detalle_programacion_medica.get(i).getDia_31());
                spDetailProgramacion.setInt(34, detalle_programacion_medica.get(i).getNro_horas_totales());
                
                
                this.ejecutarSQLsp(spDetailProgramacion, con);
                
                idDetalle_prgra++;
            }
            sql="update correlativo set numero=numero+1 where tabla='programacion_medica'";
            PreparedStatement spActCorr=con.prepareStatement(sql);
            this.ejecutarSQLsp(spActCorr, con);
            
            sql="update correlativo set numero=numero+1 where tabla='detalle_programacion_medica'";
            PreparedStatement spActCorr2=con.prepareStatement(sql);
            this.ejecutarSQLsp(spActCorr2, con);
            
            con.commit();
            con.close();
            
        }else{
            throw new Exception("no se ha congigurado el correlativo para la tabla programacion medica");
        }
        return true;
    }
         public boolean editar()throws Exception{
        String sql="select * from f_generar_correlativo('programacion_medica') as numero";
        ResultSet res=this.ejecutarSqlSelect(sql);
        if (res.next()) {
            int nroProgramacion_medica=res.getInt("numero");
            this.setId_programacion_medica(nroProgramacion_medica);
            
            Connection con=this.abrirConexion();
            con.setAutoCommit(false);
            sql="UPDATE programacion_medica SET  numhoras_total=? WHERE id_programacion_medica=?;";
            PreparedStatement spCompra=con.prepareStatement(sql);
             spCompra.setInt(1, this.getNumhoras_total());
            spCompra.setInt(2, this.getId_programacion_medica());

            this.ejecutarSQLsp(spCompra, con);
                String sql2="select * from f_generar_correlativo('detalle_programacion_medica') as numero2";
                ResultSet res2=this.ejecutarSqlSelect(sql2);
                int idDetalle_prgra;
            if (res2.next()) {
                idDetalle_prgra=res2.getInt("numero2");
            }else{
                idDetalle_prgra=res2.getInt("numero2");
                }
            
            for (int i = 0; i < detalle_programacion_medica.size(); i++) {

                sql="INSERT INTO detalle_programacion_medica( id_programacion_medica, id_actividad, dia_1, dia_2, dia_3, dia_4,"
                        + " dia_5,dia_6, dia_7, dia_8, dia_9, dia_10, dia_11, dia_12, dia_13, dia_14,dia_15, dia_16,"
                        + " dia_17, dia_18, dia_19, dia_20, dia_21, dia_22, dia_23, dia_24, dia_25, dia_26, dia_27,"
                        + " dia_28, dia_29, dia_30, dia_31, nro_horas_totales)"
                        + "VALUES (?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                PreparedStatement spDetailProgramacion=con.prepareStatement(sql);
                spDetailProgramacion.setInt(1, this.id_programacion_medica);
                spDetailProgramacion.setInt(2, detalle_programacion_medica.get(i).getId_actividad());
                spDetailProgramacion.setInt(3, detalle_programacion_medica.get(i).getDia_1());
                spDetailProgramacion.setInt(4, detalle_programacion_medica.get(i).getDia_2());
                spDetailProgramacion.setInt(5, detalle_programacion_medica.get(i).getDia_3());
                spDetailProgramacion.setInt(6, detalle_programacion_medica.get(i).getDia_4());
                spDetailProgramacion.setInt(7, detalle_programacion_medica.get(i).getDia_5());
                spDetailProgramacion.setInt(8, detalle_programacion_medica.get(i).getDia_6());
                spDetailProgramacion.setInt(9, detalle_programacion_medica.get(i).getDia_7());
                spDetailProgramacion.setInt(10, detalle_programacion_medica.get(i).getDia_8());
                spDetailProgramacion.setInt(11, detalle_programacion_medica.get(i).getDia_9());
                spDetailProgramacion.setInt(12, detalle_programacion_medica.get(i).getDia_10());
                spDetailProgramacion.setInt(13, detalle_programacion_medica.get(i).getDia_11());
                spDetailProgramacion.setInt(14, detalle_programacion_medica.get(i).getDia_12());
                spDetailProgramacion.setInt(15, detalle_programacion_medica.get(i).getDia_13());
                spDetailProgramacion.setInt(16, detalle_programacion_medica.get(i).getDia_14());
                spDetailProgramacion.setInt(17, detalle_programacion_medica.get(i).getDia_15());
                spDetailProgramacion.setInt(18, detalle_programacion_medica.get(i).getDia_16());
                spDetailProgramacion.setInt(19, detalle_programacion_medica.get(i).getDia_17());
                spDetailProgramacion.setInt(20, detalle_programacion_medica.get(i).getDia_18());
                spDetailProgramacion.setInt(21, detalle_programacion_medica.get(i).getDia_19());
                spDetailProgramacion.setInt(22, detalle_programacion_medica.get(i).getDia_20());
                spDetailProgramacion.setInt(23, detalle_programacion_medica.get(i).getDia_21());
                spDetailProgramacion.setInt(24, detalle_programacion_medica.get(i).getDia_22());
                spDetailProgramacion.setInt(25, detalle_programacion_medica.get(i).getDia_23());
                spDetailProgramacion.setInt(26, detalle_programacion_medica.get(i).getDia_24());
                spDetailProgramacion.setInt(27, detalle_programacion_medica.get(i).getDia_25());
                spDetailProgramacion.setInt(28, detalle_programacion_medica.get(i).getDia_26());
                spDetailProgramacion.setInt(29, detalle_programacion_medica.get(i).getDia_27());
                spDetailProgramacion.setInt(30, detalle_programacion_medica.get(i).getDia_28());
                spDetailProgramacion.setInt(31, detalle_programacion_medica.get(i).getDia_29());
                spDetailProgramacion.setInt(32, detalle_programacion_medica.get(i).getDia_30());
                spDetailProgramacion.setInt(33, detalle_programacion_medica.get(i).getDia_31());
                spDetailProgramacion.setInt(34, detalle_programacion_medica.get(i).getNro_horas_totales());
                
                
                this.ejecutarSQLsp(spDetailProgramacion, con);
                
                idDetalle_prgra++;
            }
            sql="update correlativo set numero=numero+1 where tabla='programacion_medica'";
            PreparedStatement spActCorr=con.prepareStatement(sql);
            this.ejecutarSQLsp(spActCorr, con);
            
            sql="update correlativo set numero=numero+1 where tabla='detalle_programacion_medica'";
            PreparedStatement spActCorr2=con.prepareStatement(sql);
            this.ejecutarSQLsp(spActCorr2, con);
            
            con.commit();
            con.close();
            
        }else{
            throw new Exception("no se ha congigurado el correlativo para la tabla programacion medica");
        }
        return true;
    }
         
         public ResultSet listar() throws Exception {
        String sql = "SELECT \n" +
                    "	PM.ID_PROGRAMACION_MEDICA AS ID,\n" +
                    "	E.DESCRIPCION AS SERVICIO,\n" +
                    "	D.DESCRIPCION AS DEPARTAMENTO,\n" +
                    "	PM.MES,\n" +
                    "	PM.\"año\",\n" +
                    "	PM.NUMHORAS_TOTAL\n" +
                    "FROM PROGRAMACION_MEDICA PM INNER JOIN SERVICIO E ON (PM.ID_SERVICIO=E.ID_SERVICIO) INNER JOIN DEPARTAMENTO D ON (PM.ID_DPTO=D.ID_DPTO)\n" +
                    "--WHERE PM.ID_MEDICO = ?";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }
         
         public ResultSet leerDatos(int idPM) throws Exception {
        String sql = "SELECT \n" +
                    "	PM.ID_PROGRAMACION_MEDICA AS ID,\n" +
                    "	E.DESCRIPCION AS SERVICIO,\n" +
                    "	D.DESCRIPCION AS DEPARTAMENTO,\n" +
                    "	PM.MES,\n" +
                    "	PM.\"año\",\n" +
                    "	PM.NUMHORAS_TOTAL\n" +
                    "FROM PROGRAMACION_MEDICA PM INNER JOIN SERVICIO E ON (PM.ID_SERVICIO=E.ID_SERVICIO) INNER JOIN DEPARTAMENTO D ON (PM.ID_DPTO=D.ID_DPTO)\n" +
                    "WHERE PM.ID_MEDICO = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idPM);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
         
         public ResultSet listarExceptop(int idPM,int anio,int mess) throws Exception {
        String sql = "SELECT \n" +
                    "	PM.ID_PROGRAMACION_MEDICA AS ID,\n" +
                    "	E.DESCRIPCION AS SERVICIO,\n" +
                    "	D.DESCRIPCION AS DEPARTAMENTO,\n" +
                    "	PM.MES,\n" +
                    "	PM.\"año\",\n" +
                    "	PM.NUMHORAS_TOTAL\n" +
                    "FROM PROGRAMACION_MEDICA PM INNER JOIN SERVICIO E ON (PM.ID_SERVICIO=E.ID_SERVICIO) INNER JOIN DEPARTAMENTO D ON (PM.ID_DPTO=D.ID_DPTO)\n" +
                    "WHERE PM.ID_MEDICO != ?"
                + "and PM.año=?"
                + "and PM.mes=?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idPM);
        sentencia.setInt(2, anio);
        sentencia.setInt(3, mess);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
         
         public ResultSet leerDatosContingencias31(int idMedico, int idPm) throws Exception {
        String sql = "SELECT \n" +
                            "  detalle_programacion_medica.id_actividad, \n" +
                            "  actividades.descripcion, \n" +
                            "  detalle_programacion_medica.nro_horas_totales, \n" +
                            "  detalle_programacion_medica.dia_1, \n" +
                            "  detalle_programacion_medica.dia_2, \n" +
                            "  detalle_programacion_medica.dia_3, \n" +
                            "  detalle_programacion_medica.dia_4, \n" +
                            "  detalle_programacion_medica.dia_5, \n" +
                            "  detalle_programacion_medica.dia_6, \n" +
                            "  detalle_programacion_medica.dia_7, \n" +
                            "  detalle_programacion_medica.dia_8, \n" +
                            "  detalle_programacion_medica.dia_9, \n" +
                            "  detalle_programacion_medica.dia_10, \n" +
                            "  detalle_programacion_medica.dia_11, \n" +
                            "  detalle_programacion_medica.dia_12, \n" +
                            "  detalle_programacion_medica.dia_13, \n" +
                            "  detalle_programacion_medica.dia_14, \n" +
                            "  detalle_programacion_medica.dia_15, \n" +
                            "  detalle_programacion_medica.dia_16, \n" +
                            "  detalle_programacion_medica.dia_17, \n" +
                            "  detalle_programacion_medica.dia_18, \n" +
                            "  detalle_programacion_medica.dia_19, \n" +
                            "  detalle_programacion_medica.dia_20, \n" +
                            "  detalle_programacion_medica.dia_21, \n" +
                            "  detalle_programacion_medica.dia_22, \n" +
                            "  detalle_programacion_medica.dia_23, \n" +
                            "  detalle_programacion_medica.dia_24, \n" +
                            "  detalle_programacion_medica.dia_25, \n" +
                            "  detalle_programacion_medica.dia_26, \n" +
                            "  detalle_programacion_medica.dia_27, \n" +
                            "  detalle_programacion_medica.dia_28, \n" +
                            "  detalle_programacion_medica.dia_29, \n" +
                            "  detalle_programacion_medica.dia_30, \n" +
                            "  detalle_programacion_medica.dia_31\n" +
                            "FROM \n" +
                            "  public.actividades, \n" +
                            "  public.detalle_programacion_medica, \n" +
                            "  public.programacion_medica, \n" +
                            "  public.medico\n" +
                            "WHERE \n" +
                            "  actividades.id_actividades = detalle_programacion_medica.id_actividad AND\n" +
                            "  detalle_programacion_medica.id_programacion_medica = programacion_medica.id_programacion_medica AND\n" +
                            "  programacion_medica.id_medico = medico.id_medico and medico.id_medico=? and programacion_medica.id_programacion_medica=?;";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idMedico);
        sentencia.setInt(2, idPm);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
         
         public ResultSet leerDatosContingencias30(int idMedico, int idPm) throws Exception {
        String sql = "SELECT \n" +
                                "  detalle_programacion_medica.id_actividad, \n" +
                                "  actividades.descripcion, \n" +
                                "  detalle_programacion_medica.nro_horas_totales, \n" +
                                "  detalle_programacion_medica.dia_1, \n" +
                                "  detalle_programacion_medica.dia_2, \n" +
                                "  detalle_programacion_medica.dia_3, \n" +
                                "  detalle_programacion_medica.dia_4, \n" +
                                "  detalle_programacion_medica.dia_5, \n" +
                                "  detalle_programacion_medica.dia_6, \n" +
                                "  detalle_programacion_medica.dia_7, \n" +
                                "  detalle_programacion_medica.dia_8, \n" +
                                "  detalle_programacion_medica.dia_9, \n" +
                                "  detalle_programacion_medica.dia_10, \n" +
                                "  detalle_programacion_medica.dia_11, \n" +
                                "  detalle_programacion_medica.dia_12, \n" +
                                "  detalle_programacion_medica.dia_13, \n" +
                                "  detalle_programacion_medica.dia_14, \n" +
                                "  detalle_programacion_medica.dia_15, \n" +
                                "  detalle_programacion_medica.dia_16, \n" +
                                "  detalle_programacion_medica.dia_17, \n" +
                                "  detalle_programacion_medica.dia_18, \n" +
                                "  detalle_programacion_medica.dia_19, \n" +
                                "  detalle_programacion_medica.dia_20, \n" +
                                "  detalle_programacion_medica.dia_21, \n" +
                                "  detalle_programacion_medica.dia_22, \n" +
                                "  detalle_programacion_medica.dia_23, \n" +
                                "  detalle_programacion_medica.dia_24, \n" +
                                "  detalle_programacion_medica.dia_25, \n" +
                                "  detalle_programacion_medica.dia_26, \n" +
                                "  detalle_programacion_medica.dia_27, \n" +
                                "  detalle_programacion_medica.dia_28, \n" +
                                "  detalle_programacion_medica.dia_29, \n" +
                                "  detalle_programacion_medica.dia_30\n" +
                                "FROM \n" +
                                "  public.actividades, \n" +
                                "  public.detalle_programacion_medica, \n" +
                                "  public.programacion_medica, \n" +
                                "  public.medico\n" +
                                "WHERE \n" +
                                "  actividades.id_actividades = detalle_programacion_medica.id_actividad AND\n" +
                                "  detalle_programacion_medica.id_programacion_medica = programacion_medica.id_programacion_medica AND\n" +
                                "  programacion_medica.id_medico = medico.id_medico and medico.id_medico=? and programacion_medica.id_programacion_medica=?;";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idMedico);
        sentencia.setInt(2, idPm);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
        
         public ResultSet leerDatosContingencias29(int idMedico, int idProgramacionMedica) throws Exception {
        String sql = "SELECT \n" +
                                "  detalle_programacion_medica.id_actividad, \n" +
                                "  actividades.descripcion, \n" +
                                "  detalle_programacion_medica.nro_horas_totales, \n" +
                                "  detalle_programacion_medica.dia_1, \n" +
                                "  detalle_programacion_medica.dia_2, \n" +
                                "  detalle_programacion_medica.dia_3, \n" +
                                "  detalle_programacion_medica.dia_4, \n" +
                                "  detalle_programacion_medica.dia_5, \n" +
                                "  detalle_programacion_medica.dia_6, \n" +
                                "  detalle_programacion_medica.dia_7, \n" +
                                "  detalle_programacion_medica.dia_8, \n" +
                                "  detalle_programacion_medica.dia_9, \n" +
                                "  detalle_programacion_medica.dia_10, \n" +
                                "  detalle_programacion_medica.dia_11, \n" +
                                "  detalle_programacion_medica.dia_12, \n" +
                                "  detalle_programacion_medica.dia_13, \n" +
                                "  detalle_programacion_medica.dia_14, \n" +
                                "  detalle_programacion_medica.dia_15, \n" +
                                "  detalle_programacion_medica.dia_16, \n" +
                                "  detalle_programacion_medica.dia_17, \n" +
                                "  detalle_programacion_medica.dia_18, \n" +
                                "  detalle_programacion_medica.dia_19, \n" +
                                "  detalle_programacion_medica.dia_20, \n" +
                                "  detalle_programacion_medica.dia_21, \n" +
                                "  detalle_programacion_medica.dia_22, \n" +
                                "  detalle_programacion_medica.dia_23, \n" +
                                "  detalle_programacion_medica.dia_24, \n" +
                                "  detalle_programacion_medica.dia_25, \n" +
                                "  detalle_programacion_medica.dia_26, \n" +
                                "  detalle_programacion_medica.dia_27, \n" +
                                "  detalle_programacion_medica.dia_28, \n" +
                                "  detalle_programacion_medica.dia_29\n" +
                                "FROM \n" +
                                "  public.actividades, \n" +
                                "  public.detalle_programacion_medica, \n" +
                                "  public.programacion_medica, \n" +
                                "  public.medico\n" +
                                "WHERE \n" +
                                "  actividades.id_actividades = detalle_programacion_medica.id_actividad AND\n" +
                                "  detalle_programacion_medica.id_programacion_medica = programacion_medica.id_programacion_medica AND\n" +
                                "  programacion_medica.id_medico = medico.id_medico and medico.id_medico=? and programacion_medica.id_programacion_medica=?;";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idMedico);
        sentencia.setInt(2, idProgramacionMedica);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
         
         public ResultSet leerDatosContingencias28(int idMedico, int idPm) throws Exception {
        String sql = "SELECT \n" +
                                "  detalle_programacion_medica.id_actividad, \n" +
                                "  actividades.descripcion, \n" +
                                "  detalle_programacion_medica.nro_horas_totales, \n" +
                                "  detalle_programacion_medica.dia_1, \n" +
                                "  detalle_programacion_medica.dia_2, \n" +
                                "  detalle_programacion_medica.dia_3, \n" +
                                "  detalle_programacion_medica.dia_4, \n" +
                                "  detalle_programacion_medica.dia_5, \n" +
                                "  detalle_programacion_medica.dia_6, \n" +
                                "  detalle_programacion_medica.dia_7, \n" +
                                "  detalle_programacion_medica.dia_8, \n" +
                                "  detalle_programacion_medica.dia_9, \n" +
                                "  detalle_programacion_medica.dia_10, \n" +
                                "  detalle_programacion_medica.dia_11, \n" +
                                "  detalle_programacion_medica.dia_12, \n" +
                                "  detalle_programacion_medica.dia_13, \n" +
                                "  detalle_programacion_medica.dia_14, \n" +
                                "  detalle_programacion_medica.dia_15, \n" +
                                "  detalle_programacion_medica.dia_16, \n" +
                                "  detalle_programacion_medica.dia_17, \n" +
                                "  detalle_programacion_medica.dia_18, \n" +
                                "  detalle_programacion_medica.dia_19, \n" +
                                "  detalle_programacion_medica.dia_20, \n" +
                                "  detalle_programacion_medica.dia_21, \n" +
                                "  detalle_programacion_medica.dia_22, \n" +
                                "  detalle_programacion_medica.dia_23, \n" +
                                "  detalle_programacion_medica.dia_24, \n" +
                                "  detalle_programacion_medica.dia_25, \n" +
                                "  detalle_programacion_medica.dia_26, \n" +
                                "  detalle_programacion_medica.dia_27, \n" +
                                "  detalle_programacion_medica.dia_28\n" +
                                "FROM \n" +
                                "  public.actividades, \n" +
                                "  public.detalle_programacion_medica, \n" +
                                "  public.programacion_medica, \n" +
                                "  public.medico\n" +
                                "WHERE \n" +
                                "  actividades.id_actividades = detalle_programacion_medica.id_actividad AND\n" +
                                "  detalle_programacion_medica.id_programacion_medica = programacion_medica.id_programacion_medica AND\n" +
                                "  programacion_medica.id_medico = medico.id_medico and medico.id_medico=? and programacion_medica.id_programacion_medica=?;";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idMedico);
        sentencia.setInt(2, idPm);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
         
     public boolean editarContingenciasMedicoActual()throws Exception{
         Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);
        
        for (int i = 0; i < detalle_programacion_medica.size(); i++) {            
            DetalleProgramacionMedica detalleActual = detalle_programacion_medica.get(i);
            
             String sql = "UPDATE public.detalle_programacion_medica\n" +
                                "   SET \n" +
                                "	dia_1=?, dia_2=?, dia_3=?, dia_4=?, dia_5=?, dia_6=?, dia_7=?, dia_8=?, dia_9=?, dia_10=?, dia_11=?, dia_12=?, dia_13=?, dia_14=?, dia_15=?, dia_16=?, \n" +
                                "	dia_17=?, dia_18=?, dia_19=?, dia_20=?, dia_21=?, dia_22=?, dia_23=?, dia_24=?, dia_25=?, dia_26=?, dia_27=?, dia_28=?, dia_29=?, dia_30=?, dia_31=?, \n" +
                                "	nro_horas_totales=?\n" +
                                " WHERE \n" +
                                "	id_programacion_medica=? and id_actividad=?;";
             
                PreparedStatement medicoActual = transaccion.prepareStatement(sql);
        
                medicoActual.setInt(1, detalleActual.getDia_1());
                medicoActual.setInt(2, detalleActual.getDia_2());
                medicoActual.setInt(3, detalleActual.getDia_3());
                medicoActual.setInt(4, detalleActual.getDia_4());
                medicoActual.setInt(5, detalleActual.getDia_5());
                medicoActual.setInt(6, detalleActual.getDia_6());
                medicoActual.setInt(7, detalleActual.getDia_7());
                medicoActual.setInt(8, detalleActual.getDia_8());
                medicoActual.setInt(9, detalleActual.getDia_9());
                medicoActual.setInt(10, detalleActual.getDia_10());
                medicoActual.setInt(11, detalleActual.getDia_11());
                medicoActual.setInt(12, detalleActual.getDia_12());
                medicoActual.setInt(13, detalleActual.getDia_13());
                medicoActual.setInt(14, detalleActual.getDia_14());
                medicoActual.setInt(15, detalleActual.getDia_15());
                medicoActual.setInt(16, detalleActual.getDia_16());
                medicoActual.setInt(17, detalleActual.getDia_17());
                medicoActual.setInt(18, detalleActual.getDia_18());
                medicoActual.setInt(19, detalleActual.getDia_19());
                medicoActual.setInt(20, detalleActual.getDia_20());
                medicoActual.setInt(21, detalleActual.getDia_21());
                medicoActual.setInt(22, detalleActual.getDia_22());
                medicoActual.setInt(23, detalleActual.getDia_23());
                medicoActual.setInt(24, detalleActual.getDia_24());
                medicoActual.setInt(25, detalleActual.getDia_25());
                medicoActual.setInt(26, detalleActual.getDia_26());
                medicoActual.setInt(27, detalleActual.getDia_27());
                medicoActual.setInt(28, detalleActual.getDia_28());
                medicoActual.setInt(29, detalleActual.getDia_29());
                medicoActual.setInt(30, detalleActual.getDia_30());
                medicoActual.setInt(31, detalleActual.getDia_31());
                medicoActual.setInt(32, detalleActual.getNro_horas_totales());
                medicoActual.setInt(33, getId_programacion_medica());       
                medicoActual.setInt(34, detalleActual.getId_actividad());
        
        this.ejecutarSql(medicoActual, transaccion);
        }     
        
        transaccion.commit();
        transaccion.close();
        
        return true;
    }    
     
      public ResultSet leerDatosMedicoActual(int codigoMedico)throws Exception{
        String sql = "select * from programacion_medica where id_medico = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigoMedico);
        ResultSet resutado = this.ejecutarSqlSelectSP(sentencia);
        return resutado;
    }
      
       public boolean editarContingenciasMedicoIntercambio()throws Exception{
         Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);
        
        for (int i = 0; i < detalle_programacion_medica.size(); i++) {            
            DetalleProgramacionMedica detalleActual = detalle_programacion_medica.get(i);
            
             String sql = "UPDATE public.detalle_programacion_medica\n" +
                                "   SET \n" +
                                "	dia_1=?, dia_2=?, dia_3=?, dia_4=?, dia_5=?, dia_6=?, dia_7=?, dia_8=?, dia_9=?, dia_10=?, dia_11=?, dia_12=?, dia_13=?, dia_14=?, dia_15=?, dia_16=?, \n" +
                                "	dia_17=?, dia_18=?, dia_19=?, dia_20=?, dia_21=?, dia_22=?, dia_23=?, dia_24=?, dia_25=?, dia_26=?, dia_27=?, dia_28=?, dia_29=?, dia_30=?, dia_31=?, \n" +
                                "	nro_horas_totales=?\n" +
                                " WHERE \n" +
                                "	id_programacion_medica=? and id_actividad=?;";
             
                PreparedStatement medicoIntercambio = transaccion.prepareStatement(sql);
        
                medicoIntercambio.setInt(1, detalleActual.getDia_1());
                medicoIntercambio.setInt(2, detalleActual.getDia_2());
                medicoIntercambio.setInt(3, detalleActual.getDia_3());
                medicoIntercambio.setInt(4, detalleActual.getDia_4());
                medicoIntercambio.setInt(5, detalleActual.getDia_5());
                medicoIntercambio.setInt(6, detalleActual.getDia_6());
                medicoIntercambio.setInt(7, detalleActual.getDia_7());
                medicoIntercambio.setInt(8, detalleActual.getDia_8());
                medicoIntercambio.setInt(9, detalleActual.getDia_9());
                medicoIntercambio.setInt(10, detalleActual.getDia_10());
                medicoIntercambio.setInt(11, detalleActual.getDia_11());
                medicoIntercambio.setInt(12, detalleActual.getDia_12());
                medicoIntercambio.setInt(13, detalleActual.getDia_13());
                medicoIntercambio.setInt(14, detalleActual.getDia_14());
                medicoIntercambio.setInt(15, detalleActual.getDia_15());
                medicoIntercambio.setInt(16, detalleActual.getDia_16());
                medicoIntercambio.setInt(17, detalleActual.getDia_17());
                medicoIntercambio.setInt(18, detalleActual.getDia_18());
                medicoIntercambio.setInt(19, detalleActual.getDia_19());
                medicoIntercambio.setInt(20, detalleActual.getDia_20());
                medicoIntercambio.setInt(21, detalleActual.getDia_21());
                medicoIntercambio.setInt(22, detalleActual.getDia_22());
                medicoIntercambio.setInt(23, detalleActual.getDia_23());
                medicoIntercambio.setInt(24, detalleActual.getDia_24());
                medicoIntercambio.setInt(25, detalleActual.getDia_25());
                medicoIntercambio.setInt(26, detalleActual.getDia_26());
                medicoIntercambio.setInt(27, detalleActual.getDia_27());
                medicoIntercambio.setInt(28, detalleActual.getDia_28());
                medicoIntercambio.setInt(29, detalleActual.getDia_29());
                medicoIntercambio.setInt(30, detalleActual.getDia_30());
                medicoIntercambio.setInt(31, detalleActual.getDia_31());
                medicoIntercambio.setInt(32, detalleActual.getNro_horas_totales());
                medicoIntercambio.setInt(33, getId_programacion_medica());       
                medicoIntercambio.setInt(34, detalleActual.getId_actividad());
        
        this.ejecutarSql(medicoIntercambio, transaccion);
        }     
        
        transaccion.commit();
        transaccion.close();
        
        return true;
    }    
        
       
    
}
