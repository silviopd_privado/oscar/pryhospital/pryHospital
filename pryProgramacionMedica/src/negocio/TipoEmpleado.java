package negocio;

import accesoDatos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class TipoEmpleado extends Conexion{
    
    private int codigoTipoEmpleado;
    private String descripcion;
    
    public static ArrayList<TipoEmpleado> listaTipoEmpleado = new ArrayList<TipoEmpleado>();

    public int getCodigoTipoEmpleado() {
        return codigoTipoEmpleado;
    }

    public void setCodigoTipoEmpleado(int codigoTipoEmpleado) {
        this.codigoTipoEmpleado = codigoTipoEmpleado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public void cargarDatosTabla()throws Exception{
        String sql ="select * from tipo_personal order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaTipoEmpleado.clear();
        while(resultado.next()){
            TipoEmpleado objTipoEmpleado = new TipoEmpleado();
            objTipoEmpleado.setCodigoTipoEmpleado(resultado.getInt("id_tipo_personal"));
            objTipoEmpleado.setDescripcion(resultado.getString("descripcion"));
            listaTipoEmpleado.add(objTipoEmpleado);
        }
    }
    
    public void llenarCombo(JComboBox objComboBox)throws Exception{
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTipoEmpleado.size(); i++) {
            TipoEmpleado item = listaTipoEmpleado.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }
}
