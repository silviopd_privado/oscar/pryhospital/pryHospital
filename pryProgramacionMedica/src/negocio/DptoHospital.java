package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class DptoHospital extends Conexion {

    private int codigoDptoHosp;
    private String nombre;

    public static ArrayList<DptoHospital> listaDptoHosp = new ArrayList<DptoHospital>();

    public int getCodigoDptoHosp() {
        return codigoDptoHosp;
    }

    public void setCodigoDptoHosp(int codigoDptoHosp) {
        this.codigoDptoHosp = codigoDptoHosp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void cargarDatosTabla() throws Exception {
        String sql = "select * from departamento order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaDptoHosp.clear();
        while (resultado.next()) {
            DptoHospital objDptoHosp = new DptoHospital();
            objDptoHosp.setCodigoDptoHosp(resultado.getInt("id_dpto"));
            objDptoHosp.setNombre(resultado.getString("descripcion"));
            listaDptoHosp.add(objDptoHosp);
        }
    }

    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaDptoHosp.size(); i++) {
            DptoHospital item = listaDptoHosp.get(i);
            objComboBox.addItem(item.nombre);
        }
    }

    public ResultSet listar() throws Exception {
        String sql = "Select * from departamento order by 2;";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);

        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }

    public String obetenerCamposFiltro() {
        String camposFiltro = "DESCRIPCION";
        return camposFiltro;
    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('departamento') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigoDpto = resultado.getInt("nc");
            this.setCodigoDptoHosp(nuevoCodigoDpto);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.departamento(id_dpto, descripcion) VALUES (?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getCodigoDptoHosp());
            spInsertar.setString(2, getNombre());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'departamento' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Departamento Hospital");
        }
        return true;
    }

    public ResultSet leerDatos(int codigoDpto) throws Exception {
        String sql = "Select * from departamento where id_dpto = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigoDpto);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE departamento SET descripcion = ? WHERE id_dpto = ?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getNombre());
        sentencia.setInt(2, this.getCodigoDptoHosp());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoDpto) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE from departamento WHERE id_dpto = ?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, codigoDpto);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

}
