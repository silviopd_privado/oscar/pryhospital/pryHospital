package accesoDatos;

import java.sql.*;

public class Conexion {

    private String controlador = "org.postgresql.Driver";
    private String cadenaConexion  = "jdbc:postgresql://localhost:5432/bd_programacion_medica_v8";
//    private String cadenaConexion = "jdbc:postgresql://localhost:5432/bd_programacion_medica_v5";
    

    private String usuario = "postgres";
    private String clave = "1234";
//    private String clave = "12345";

    private Connection conexion;

    protected Connection abrirConexion() throws Exception { // throws exception traslada el error a la capa sgt: datos, negocio y presentacion
        Class.forName(controlador);
        conexion = DriverManager.getConnection(cadenaConexion, usuario, clave);
        return conexion;
    }

    protected void cerrarConexion(Connection con) throws Exception {
        con.close();
        con = null;
    }

    protected ResultSet ejecutarSqlSelect(String sql) throws Exception { // solo para hacer select en sql para almacenar los registros
        Statement sentencia = null;
        ResultSet resultado = null;

        sentencia = this.abrirConexion().createStatement();
        resultado = sentencia.executeQuery(sql);

        this.cerrarConexion(conexion);
        return resultado;
    }

    protected ResultSet ejecutarSqlSelectSP(PreparedStatement sentencia) throws Exception {
        ResultSet resultado = null;
        resultado = sentencia.executeQuery();

        this.cerrarConexion(conexion);
        return resultado;
    }

    protected int ejecutarSql(PreparedStatement sentencia, Connection transaccion) throws Exception {
        int resultado = 0;
        try {
            resultado = sentencia.executeUpdate(); //ejecutar las operaciones: insert, update y delete
        } catch (Exception e) {
            transaccion.rollback(); //abortar la transsaccion, o cancelar la transacciom
            throw e; //si ha ocurrido algun error, primero abortar y lanzar el error a la sgt capa
        }
        return resultado;
    }
    
    protected int ejecutarSQLsp(PreparedStatement sentencia,Connection con) throws Exception{
        int resultado=0;
        try {
            resultado =sentencia.executeUpdate();
        } catch (Exception e) {
            con.rollback();
            throw  e;
        }
        return resultado;
    }

}
